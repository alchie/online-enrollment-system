<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateStudentsEnrolledAttributes1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students_enrolled', function (Blueprint $table) {
            $table->bigInteger('residential')->nullable();
            $table->bigInteger('provincial')->nullable();
            $table->bigInteger('academic')->nullable();
            $table->bigInteger('father')->nullable();
            $table->bigInteger('mother')->nullable();
            $table->bigInteger('guardian')->nullable();
            $table->string('status')->nullable()->default('new');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('residential');
            $table->dropColumn('provincial');
            $table->dropColumn('academic');
            $table->dropColumn('father');
            $table->dropColumn('mother');
            $table->dropColumn('guardian');
            $table->dropColumn('status');
        });
    }
}
