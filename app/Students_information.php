<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Students_information extends Model
{
    protected $table = 'students_information';

    public function addresses() {
        return $this->hasMany('App\Students_address', 'student_id', 'id');
    }

    public function academics() {
        return $this->hasMany('App\Students_academic', 'student_id', 'id');
    }

    public function parents() {
        return $this->hasMany('App\Students_parents', 'student_id', 'id');
    }

    public function claim_codes() {
        return $this->hasMany('App\Claim_codes', 'student_id', 'id');
    }
}
