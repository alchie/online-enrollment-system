<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Students_enrolled extends Model
{
    protected $table = 'students_enrolled';
    
    public function information() {
        return $this->hasOne('App\Students_information', 'id', 'student_id');
    }

    public function user() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function metas() {
        return $this->hasMany('App\Students_enrolled_meta', 'enrolled_id', 'id');
    }
}
