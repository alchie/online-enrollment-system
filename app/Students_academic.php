<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Students_academic extends Model
{
    protected $table = 'students_academics';

    public function information() {
        return $this->belongsTo('App\Students_information', 'id', 'student_id');
    }

}
