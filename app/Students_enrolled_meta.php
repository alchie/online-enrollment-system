<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Students_enrolled_meta extends Model
{
    protected $table = 'students_enrolled_meta';

    public function enrolled() {
        return $this->belongsTo('App\Students_enrolled', 'id', 'enrolled_id');
    }
}
