<?php

namespace App\Http\Middleware;
class TemplateData
{

    private $data = array();

    public function set($key, $value=NULL)
    {
        if( is_array($key) ) {
            foreach($key as $k=>$v) {
                $this->data[$k] = $v;
            }
        } else {
            $this->data[$key] = $value;
        }
    }
    
    public function get($key)
    {
        return (isset($this->data[$key])) ? $this->data[$key] : null;
    }

    public function prepend($key, $value)
    {
        $current_value = $this->get($key);
        $this->set($key, $value . $current_value );  
    }
    
    public function append($key, $value)
    {
        $current_value = $this->get($key);
        $this->set($key, $current_value . $value );        
    }

    public function get_data() {
        return $this->data;
    }
    
}
