<?php

namespace App\Http\Middleware;

use Closure;

use App\Users_settings;

class SettingsTheme
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if( ! session()->get('system-started') ) {
            
            $user = $request->user();

            $users_settings = Users_settings::where([
                ['user_id', '=', $user->id],
                ['settings_module', '=', 'theme'],
            ])->get();
            $theme = [];
            foreach($users_settings as $setting) {
                
                switch($setting->settings_key) {
                    case 'navbar':
                        session([ 'theme-navbar' => $setting->settings_value ]);
                    break;
                    case 'accent':
                        session([ 'theme-accent' => $setting->settings_value ]);
                    break;
                    case 'sidebar':
                        session([ 'theme-sidebar' => $setting->settings_value ]);
                    break;
                    case 'brand-logo':
                        session([ 'theme-brand-logo' => $setting->settings_value ]);
                    break;
                    case 'bg':
                        session([ 'theme-bg' => $setting->settings_value ]);
                    break;
                    default:
                        $theme[] = $setting->settings_key;
                    break;
                }
            } 
            session(['theme'=>$theme]);
            session(['system-started'=>true]);
        }
        
        if( ! session()->get('user_permissions') ) {

            $user = $request->user();
            
            $user_permissions = [];
            $from_db = Users_settings::where([['user_id', '=', $user->id],['settings_module','=',"user_permissions"]])->get();
            foreach($from_db as $db) {
                $user_permissions[$db->settings_key] = $db->settings_value;
            }
            session(['user_permissions'=>$user_permissions]);
        }
        
        return $next($request);
    }

    private function _set_setting($module, $key, $default='')
    {
        $user = auth()->user();
        
        $users_settings = Users_settings::where([
            ['user_id', '=', $user->id],
            ['settings_module', '=', 'theme'],
            ['settings_key', '=', $key],
        ])->first();

        if( $users_settings ) {
            session([ $module . '-' . $key => $users_settings->settings_value ]);
        } else {
            session([ $module . '-' . $key => $default ]);
        }
    }
}
