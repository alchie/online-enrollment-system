<?php

namespace App\Http\Controllers\Manage;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\School_fees;

class SchoolFeesController extends BaseController
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->template_data->set('current_controller', 'school_fees');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = School_fees::where('school_year', config('enrollment.current_school_year'));
        $total_fees = [];
        
        foreach($items->get() as $item) {
            if( isset( $total_fees[$item->grade_level] ) ) {
                $total_fees[$item->grade_level] += $item->amount;
            } else {
                $total_fees[$item->grade_level] = $item->amount;
            }
        }
        $this->template_data->set('total_fees', $total_fees);
        return view('manage.school_fees.index')->with( $this->template_data->get_data() );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($grade_level)
    {
        $this->template_data->set('grade_level', $grade_level);

        $items = School_fees::where('school_year', config('enrollment.current_school_year'))
        ->where( 'grade_level', $grade_level );

        $this->template_data->set('total_fees', $items->sum('amount') );

        $fees = [];
        foreach($items->get() as $item) {
            $fees[$item->month] = [ 'amount' => $item->amount, 'year' => $item->year ];
        }
        $this->template_data->set('fees', $fees );

        return view('manage.school_fees.edit')->with( $this->template_data->get_data() );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->isAllowed('school_fees', 'update');

        $validator = Validator::make($request->all(), [
            'amount' => 'required|array',
            'year' => 'required|array'
        ]);

        if ($validator->fails()) {

            return redirect("manage/school_fees/{$id}/edit")->withErrors($validator)->withInput();

        } else {

            $months = $request->input('amount');
            $year = $request->input('year');
            foreach($months as $month => $amount) {
                if( $amount ) {
                    
                    $item = School_fees::where('grade_level', $id)
                    ->where('school_year', config('enrollment.current_school_year'))
                    ->where('month', $month);

                    if( $item->exists() ) {
                        $item->update([ 'amount' => $amount, 'year' => ( (isset($year[$month]) ) ? $year[$month] : '') ]);
                    } else {
                        $item = new School_fees;
                        $item->school_year = config('enrollment.current_school_year');
                        $item->month = $month;
                        $item->year = (isset($year[$month])) ? $year[$month] : '';
                        $item->grade_level = $id;
                        $item->amount = $amount;
                        $item->save();
                    }
                }
                
            }
            return redirect("manage/school_fees/{$id}/edit");

        }
    }

}
