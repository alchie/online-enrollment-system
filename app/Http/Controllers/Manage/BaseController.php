<?php

namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;

class BaseController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware(['auth','check.admin','settings.theme']);

        $this->template_data->set('search_form_show', false);
        $this->template_data->set('search_form_placeholder', 'Search');


    }

    public function isAllowed($module, $actions, $bool=false)
    {
        
        return true; // remove on production

        if( is_array($actions) ) {
            $primes = [];
            foreach($actions as $action) {
                $primes[] = config('cms.actions')[$action]['prime'];
            }
            $prime = array_product($primes);
        } else {
            $prime = config('cms.actions')[$actions]['prime'];
        }

        $session = session()->get('user_permissions');
        if( ( isset( $session[$module] ) ) && 
        ( $session[$module] % $prime ) == 0 ) {
            return true;
        } else {
            if( $bool ) {
                return false;
            } else {
                return abort(403, 'Unauthorized action.');
            }
        }

    }

}
