<?php

namespace App\Http\Controllers\Manage;

use Illuminate\Http\Request;

use App\Students_enrolled;
use App\Students_information;

class EnrolleesController extends BaseController
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->template_data->set('current_controller', 'enrollees');
        $this->template_data->set('search_form_show', true);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Students_enrolled::orderBy('students_information.lastname')
        ->join('users_students', 'users_students.id','=','students_enrolled.student_id')
        ->join('students_information', 'students_information.id','=','users_students.student_id');
        
        if( request()->query('q') ) {
            $items->whereRaw("students_information.lastname LIKE '%".request()->query('q')."%'");
            $items->orWhereRaw("students_information.firstname LIKE '%".request()->query('q')."%'");
            $items->orWhereRaw("students_information.middlename LIKE '%".request()->query('q')."%'");
        }

        $this->template_data->set('items', $items->paginate(10) );
        return view('manage.enrollees.index')->with( $this->template_data->get_data() );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('manage.enrollees.create')->with( $this->template_data->get_data() );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->isAllowed('students', 'create');

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'description' => 'required|string'
        ]);

        if ($validator->fails()) {

            return redirect('manage/students/create')->withErrors($validator)->withInput();

        } else {

            $item = new Students_information;
            $item->created_by = $request->user()->id;
            $item->modified_by = $request->user()->id;
            $item->name = $request->input('name');
            $item->description = $request->input('description');
            if( $item->save() ) {
                return redirect('/manage/students/' . $item->id . '/edit');
            }
            return redirect('/manage/students');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('manage.enrollees.show')->with( $this->template_data->get_data() );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->template_data->set('current_item', Students_information::find($id));
        return view('manage.enrollees.edit')->with( $this->template_data->get_data() );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->isAllowed('students', 'update');

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'description' => 'required|string'
        ]);

        if ($validator->fails()) {

            return redirect("manage/students/{$id}/edit")->withErrors($validator)->withInput();

        } else {

            $item = Students_information::find($id);
            $item->modified_by = $request->user()->id;
            $item->name = $request->input('name');
            $item->description = $request->input('description');
            if( $item->save() ) {
                return redirect('/manage/students/' . $item->id . '/edit');
            }
            return redirect("manage/students/{$id}/edit");

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Students_information::find($id)->delete();
        return redirect("manage/students");
    }
}
