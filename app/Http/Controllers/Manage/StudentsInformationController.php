<?php

namespace App\Http\Controllers\Manage;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\Students_information;
use App\Claim_codes;

class StudentsInformationController extends BaseController
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->template_data->set('current_controller', 'students');
        $this->template_data->set('search_form_show', true);
        $this->template_data->set('current_key', 'edit');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Students_information::orderBy('lastname');
        if( request()->query('q') ) {
            $items->whereRaw("lastname LIKE '%".request()->query('q')."%'");
            $items->orWhereRaw("firstname LIKE '%".request()->query('q')."%'");
            $items->orWhereRaw("middlename LIKE '%".request()->query('q')."%'");
        }
        $this->template_data->set('items', $items->paginate(10) );
        return view('manage.students.index')->with( $this->template_data->get_data() );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('manage.students.create')->with( $this->template_data->get_data() );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->isAllowed('students', 'create');

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'description' => 'required|string'
        ]);

        if ($validator->fails()) {

            return redirect('manage/students/create')->withErrors($validator)->withInput();

        } else {

            $item = new Students_information;
            $item->created_by = $request->user()->id;
            $item->modified_by = $request->user()->id;
            $item->name = $request->input('name');
            $item->description = $request->input('description');
            if( $item->save() ) {
                return redirect('/manage/students/' . $item->id . '/edit');
            }
            return redirect('/manage/students');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->generate_claim_codes();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->template_data->set('current_key', 'edit');
        $this->template_data->set('student_id', $id);
        $this->template_data->set('current_item', Students_information::find($id));
        return view('manage.students.edit')->with( $this->template_data->get_data() );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->isAllowed('students', 'update');

        $validator = Validator::make($request->all(), [
            'lastname' => 'required|string|max:255',
            'firstname' => 'required|string|max:255',
            'middlename' => 'required|string|max:255',
            'birthday' => 'required|date',
            'birthplace' => 'required|string|max:255',
            'religion' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {

            return redirect("manage/students/{$id}/edit")->withErrors($validator)->withInput();

        } else {

            $item = Students_information::find($id);
            $item->lastname = $request->input('lastname');
            $item->firstname = $request->input('firstname');
            $item->middlename = $request->input('middlename');
            $item->birthday = $request->input('birthday');
            $item->birthplace = $request->input('birthplace');
            $item->religion = $request->input('religion');

            if( $item->save() ) {
                return redirect('/manage/students/' . $item->id . '/edit');
            }
            return redirect("manage/students/{$id}/edit");

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Students_information::find($id)->delete();
        return redirect("manage/students");
    }

    /**
     * Display the specified resource.
     *
     */
    public function generate_claim_codes()
    {
        $students = Students_information::orderBy('id')->get();
        foreach($students as $student) {
            
            $hash = Hash::make( $student->id . time() );
            $hash = str_replace('$2y$10$', '', $hash);
            $hash = str_replace('/', '', $hash);
            $hash = str_replace('.', '', $hash);
            $hash = substr($hash, 0, 6);
            $claim_code = new Claim_codes;
            $claim_code->student_id = $student->id;
            $claim_code->code = $hash;
            $claim_code->save();
        }
        return redirect('manage/students');
    }

    /**
     * Display the specified resource.
     *
     */
    public function generate_claim_code($id)
    {
            $hash = Hash::make( $id . time() );
            $hash = str_replace('$2y$10$', '', $hash);
            $hash = str_replace('/', '', $hash);
            $hash = str_replace('.', '', $hash);
            $hash = substr($hash, 0, 6);
            $claim_code = new Claim_codes;
            $claim_code->student_id = $id;
            $claim_code->code = $hash;
            $claim_code->save();
        
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function claim_codes($id)
    {
        $this->template_data->set('current_key', 'claim_codes');
        $this->template_data->set('student_id', $id);
        $this->template_data->set('current_item', Students_information::find($id));
        return view('manage.students.claim_codes')->with( $this->template_data->get_data() );
    }

}
