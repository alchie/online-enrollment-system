<?php

namespace App\Http\Controllers\Manage;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Students_enrolled;
use App\Students_information;
use App\Users_payments;

class DashboardController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->template_data->set('current_controller', 'dashboard');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $this->template_data->set('enrollees', Students_enrolled::count() );
        $this->template_data->set('students', Students_information::count() );
        $this->template_data->set('payments', Users_payments::count() );
        $this->template_data->set('total_payments', Users_payments::sum('amount') );
        return view('manage.dashboard.index')->with( $this->template_data->get_data() );
    }
}
