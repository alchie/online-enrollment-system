<?php

namespace App\Http\Controllers\Manage;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Students_information;
use App\Claim_codes;

class StudentsAcademicsController extends BaseController
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->template_data->set('current_controller', 'students');
        $this->template_data->set('search_form_show', true);
        $this->template_data->set('current_key', 'academics');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $this->template_data->set('student_id', $id);
        $this->template_data->set('current_item', Students_information::find($id));
        return view('manage.students.academics')->with( $this->template_data->get_data() );
    }

}
