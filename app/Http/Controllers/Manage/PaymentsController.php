<?php

namespace App\Http\Controllers\Manage;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Students_enrolled;
use App\Students_information;
use App\Users_payments;

class PaymentsController extends BaseController
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->template_data->set('current_controller', 'payments');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Users_payments::orderByDesc('id')->paginate(10);
        $this->template_data->set('items', $items );
        return view('manage.payments.index')->with( $this->template_data->get_data() );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->template_data->set('current_item', Users_payments::find($id));
        return view('manage.payments.edit')->with( $this->template_data->get_data() );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->isAllowed('students', 'update');

        $validator = Validator::make($request->all(), [
            'status' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {

            return redirect("manage/payments/{$id}/edit")->withErrors($validator)->withInput();

        } else {

            $item = Users_payments::find($id);
            $item->status = $request->input('status');
            if( $item->save() ) {
                return redirect('/manage/payments');
            }
            return redirect("manage/payments/{$id}/edit");

        }
    }

}
