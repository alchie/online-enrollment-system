<?php

namespace App\Http\Controllers\Manage;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use App\User;
use App\Users_settings;
use App\Users_payments;
use App\Users_reservations;

class UsersController extends BaseController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
        $this->template_data->set('current_controller', 'users');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $this->isAllowed('users', 'read');

        $this->template_data->set('search_form_show', true);
        $this->template_data->set('search_form_placeholder', 'Search Users');

        $users = User::orderBy('name');
        $users->whereRaw('name LIKE "%'.request()->query('q').'%"');
        $users->orWhereRaw('email LIKE "%'.request()->query('q').'%"');
        $this->template_data->set('items', $users->paginate(10) );
        return view('manage.users.index')->with( $this->template_data->get_data() );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->isAllowed('users', 'create');
        return view('manage.users.create')->with( $this->template_data->get_data() );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->isAllowed('users', 'create');
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ]);

        if ($validator->fails()) {

            return redirect('/manage/users/create')->withErrors($validator)->withInput();

        } else {

            $user = new User;
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = Hash::make($request->input('password'));
            if( $user->save() ) {
                return redirect('/manage/users/' . $user->id . '/edit');
            }
            return redirect('/manage/users');

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->isAllowed('users', 'read');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->isAllowed('users', 'update');
        $this->template_data->set('user', User::find($id));

        $user_permissions = [];
        $from_db = Users_settings::where([['user_id', '=', $id],['settings_module','=',"user_permissions"]])->get();
        foreach($from_db as $db) {
            $user_permissions[$db->settings_key] = $db->settings_value;
        }
        $this->template_data->set('user_permissions', $user_permissions);
        return view('manage.users.edit')->with( $this->template_data->get_data() );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->isAllowed('users', 'update');
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
        ]);

        if ($validator->fails()) {

            return redirect("/manage/users/{$id}/edit")->withErrors( $validator )->withInput();

        } else {

            $user = User::find($id);
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            if(
                ( $request->input('password') != '' ) &&
                ( $request->input('password_confirmation') != '' ) &&
                ( $request->input('password') === $request->input('password_confirmation') )
            ) {
                $user->password = Hash::make($request->input('password'));
            }
            if( $user->save() ) {
                return redirect('/manage/users/' . $user->id . '/edit');
            }
            return redirect('/manage/users');

        }
    }

    /**
     * Update user permissions.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function permissions(Request $request, $id)
    {
        $this->isAllowed('users', 'update');
        $this->reset_permissions( $id );
        if( $request->input('module') ) {
            foreach($request->input('module') as $module=>$primes) {
                $this->save_permissions( $id, $module, array_product($primes) );
            }
        }
        return redirect("/manage/users/{$id}/edit");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->isAllowed('users', 'delete');
    }

    /**
     * Reset Permissions
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    private function reset_permissions($user_id)
    {
        DB::table('users_settings')
        ->where([
            ['user_id', '=', $user_id],
            ['settings_module', '=', 'user_permissions'],
        ])->delete();

    }

    /**
     * Save Permissions
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    private function save_permissions($user_id, $key, $value)
    {
        $user_settings = new Users_settings;
        $user_settings->user_id = $user_id;
        $user_settings->settings_module = 'user_permissions';
        $user_settings->settings_key = $key;
        $user_settings->settings_value = $value;
        $user_settings->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function save_admin(Request $request, $id)
    {
        $this->isAllowed('users', 'update');

        $user = User::find($id);
        $user->is_admin = ($request->input('is_admin')) ? true : false;
        if( $user->save() ) {
            return redirect('/manage/users/' . $user->id . '/edit');
        }
        return redirect('/manage/users');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function payments($id)
    {
        
        $this->isAllowed('users', 'read');

        $this->template_data->set('items', Users_payments::orderByDesc('date_deposited')->whereRaw("created_by={$id}")->paginate(10) );
        return view('manage.users.payments')->with( $this->template_data->get_data() );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit_payment($id)
    {
        
        $this->isAllowed('users', 'update');
        $payment = Users_payments::find($id);
        $this->template_data->set('payment', $payment);

        if( $payment->payment_type == 'buy_tokens') {
            $this->template_data->set('tokens', Users_reservations::orderByDesc('id')->whereRaw("payment_id={$id}")->get() );
        }

        return view('manage.users.edit_payment')->with( $this->template_data->get_data() );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function change_status(Request $request, $id)
    {
        $this->isAllowed('users', 'update');

        $payment = Users_payments::find($id);
        $payment->status = $request->input('status');
        if( $payment->save() ) {
            if( $payment->status == 'verified') {
                return redirect("/manage/users/{$payment->id}/edit_payment");
            }
        }
        return redirect("/manage/users/{$payment->created_by}/payments");

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function generate_tokens(Request $request, $id)
    {
        $this->isAllowed('users', 'update');
        $payment = Users_payments::find($id);
        for($i=10;$i>0;$i--) {
            $token = new Users_reservations;
            $token->created_by = $request->user()->id;
            $token->modified_by = $request->user()->id;
            $token->user_id = $payment->created_by;
            $token->payment_id = $id;
            
            $hash = Hash::make( $i . time() . $id . $request->user()->id . $payment->created_by );
            //$hash = str_replace('/', '', $hash);
            //$hash = str_replace('$2y$10$', '', $hash);

            $token->token = $hash;
            $token->save();
        }
        return redirect("/manage/users/{$id}/edit_payment");

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reservations($id)
    {
        
        $this->isAllowed('users', 'read');

        $this->template_data->set('items', Users_reservations::orderByDesc('created_at')->whereRaw("user_id={$id}")->paginate(10) );
        return view('manage.users.reservations')->with( $this->template_data->get_data() );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function upgrade_account_level1(Request $request, $id)
    {
        $this->isAllowed('users', 'update');

        $payment = Users_payments::find($id);

        if( ($payment->payment_type=='account_upgrade') && ($payment->status=='verified') ) {
            $user = User::find( $payment->created_by );
            $user->account_level = 1;
            if( $user->save() ) {
                return redirect('/manage/users/' . $user->id . '/payments');
            }
        }
        return redirect('/manage/users');

    }

}
