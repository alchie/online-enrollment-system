<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Support\Facades\DB;
use App\Users_settings;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;
    protected function redirectTo()
    {
        return RouteServiceProvider::HOME;
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        
        if ( Auth::attempt($credentials) ) {
            // Authentication passed...
            $this->handleTheme();
            return redirect()->intended( RouteServiceProvider::HOME );
        } 

        return redirect( "/login" )->withErrors(['login' => 'Unable to login!']);
    }

    /**
     * Handle an incoming request.
     *
     */
    public function handleTheme()
    {
        
        if( ! session()->get('system-started') ) {
            
            $user = auth()->user();

            $users_settings = Users_settings::where([
                ['user_id', '=', $user->id],
                ['settings_module', '=', 'theme'],
            ])->get();
            $theme = [];
            foreach($users_settings as $setting) {
                
                switch($setting->settings_key) {
                    case 'navbar':
                        session([ 'theme-navbar' => $setting->settings_value ]);
                    break;
                    case 'accent':
                        session([ 'theme-accent' => $setting->settings_value ]);
                    break;
                    case 'sidebar':
                        session([ 'theme-sidebar' => $setting->settings_value ]);
                    break;
                    case 'brand-logo':
                        session([ 'theme-brand-logo' => $setting->settings_value ]);
                    break;
                    case 'bg':
                        session([ 'theme-bg' => $setting->settings_value ]);
                    break;
                    default:
                        $theme[] = $setting->settings_key;
                    break;
                }
            } 
            session(['theme'=>$theme]);
            session(['system-started'=>true]);
        }
        
        if( ! session()->get('user_permissions') ) {

            $user = auth()->user();
            
            $user_permissions = [];
            $from_db = Users_settings::where([['user_id', '=', $user->id],['settings_module','=',"user_permissions"]])->get();
            foreach($from_db as $db) {
                $user_permissions[$db->settings_key] = $db->settings_value;
            }
            session(['user_permissions'=>$user_permissions]);
        }
        
    }
}
