<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Students_academic;
use App\Students_information;

class StudentsAcademicController extends BaseController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->template_data->set('current_controller', 'students');
        $this->template_data->set( 'current_key', 'academic' );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        
        $this->template_data->set( 'student', Students_information::find($id) );

        return view('user.students.academic_information')->with( $this->template_data->get_data() );

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'last_attended' => 'required|string',
            'grade_level' => 'required|string',
            'address' => 'required|string',
            'school_year' => 'required|string',
        ]);

        if ($validator->fails()) {

            return redirect("/students/{$id}/academic")->withErrors( $validator )->withInput();

        } else {

            $info = new Students_academic;
            $info->student_id = $id;
            $info->last_attended = $request->input('last_attended');
            $info->address = $request->input('address');
            $info->grade_level = $request->input('grade_level');
            $info->school_year = $request->input('school_year');
            $info->save();

            return redirect("/students/{$id}/academic");

        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Students_academic  $students_academic
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $academic = Students_academic::find($id);
        $this->template_data->set( 'student_id', $academic->student_id );
        $this->template_data->set( 'academic', $academic );
        return view('user.students.edit_academic')->with( $this->template_data->get_data() );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Students_academic  $students_academic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'last_attended' => 'required|string',
            'grade_level' => 'required|string',
            'address' => 'required|string',
            'school_year' => 'required|string',
        ]);

        if ($validator->fails()) {

            return redirect("/students/{$id}/edit_academic")->withErrors( $validator )->withInput();

        } else {

            $info = Students_academic::find($id);
            $info->last_attended = $request->input('last_attended');
            $info->address = $request->input('address');
            $info->grade_level = $request->input('grade_level');
            $info->school_year = $request->input('school_year');
            $info->save();

            return redirect("/students/{$info->student_id}/academic");

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Students_academic  $students_academic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Students_academic $students_academic)
    {
        //
    }
}
