<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use App\User;
use App\Users_student;
use App\Students_enrolled;
use App\Students_enrolled_meta;
use App\Students_address;
use App\Students_academic;
use App\Students_parents;
use App\Users_payments;
use App\School_fees;

class EnrollmentController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->template_data->set('current_controller', 'enrollment');
        $this->template_data->set('current_key', 'edit');
    }

    /**
     * Show the application enrollment.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $unenrolled = Users_student::whereRaw('user_id='. auth()->user()->id)
        ->select('*')
        ->whereRaw('((SELECT COUNT(*) FROM students_enrolled WHERE students_enrolled.student_id=users_students.id AND students_enrolled.school_year="'.config('enrollment.current_school_year').'")=0)')
        ->count();
        $this->template_data->set('unenrolled', $unenrolled );

        $students = Students_enrolled::whereRaw('students_enrolled.user_id='. auth()->user()->id)
        ->select('*')
        ->selectSub('students_enrolled.id', 'enrolled_id')
        ->join('users_students', 'users_students.id','=','students_enrolled.student_id')
        ->join('students_information', 'students_information.id','=','users_students.student_id')
        ->get();
        $this->template_data->set('students', $students );
        return view('user.enrollment.index')->with( $this->template_data->get_data() );
    }

    /**
     * Show the application enrollment.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function enroll()
    {
        $students = Users_student::whereRaw('user_id='. auth()->user()->id)
        ->join('students_information', 'students_information.id','=','users_students.student_id')
        ->select('*')
        ->selectSub('users_students.id', 'us_id')
        //->selectSub('(SELECT COUNT(*) FROM students_enrolled WHERE students_enrolled.student_id=users_students.student_id AND students_enrolled.school_year="'.config('enrollment.current_school_year').'")','enrolled')
        ->whereRaw('((SELECT COUNT(*) FROM students_enrolled WHERE students_enrolled.student_id=users_students.id AND students_enrolled.school_year="'.config('enrollment.current_school_year').'")=0)')
        ->get();

        if( $students->count() == 0 ) {
            return redirect("/enrollment");
        }
        $this->template_data->set('students', $students );
        return view('user.enrollment.enroll')->with( $this->template_data->get_data() );
    }

    /**
     * Show the application enrollment.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store_enroll(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'student_id' => 'required|integer',
            'grade_level' => 'required|string|max:3',
        ]);

        if ($validator->fails()) {

            return redirect("/enrollment/enroll")->withErrors( $validator )->withInput();

        } else {

            $enroll = new Students_enrolled;
            $enroll->user_id = $request->user()->id;
            $enroll->student_id = $request->input('student_id');
            $enroll->grade_level = $request->input('grade_level');
            $enroll->status = $request->input('student_status');
            $enroll->school_year = config('enrollment.current_school_year');
            if( $enroll->save() ) {
                return redirect("/enrollment/{$enroll->id}/edit");
            }
            return redirect('/enrollment/enroll');

        }
    }

    /**
     * Show the application enrollment.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function preview($id)
    {
        $this->template_data->set('current_key', 'preview' );
        $this->template_data->set('enrolled_id', $id );
        return view('user.enrollment.preview')->with( $this->template_data->get_data() );
    }

    /**
     * Show the application enrollment.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function school_fees($id)
    {
        $this->template_data->set('current_key', 'school_fees' );
        $this->template_data->set('enrolled_id', $id );

        $enrolled = Students_enrolled::whereRaw('students_enrolled.user_id='. auth()->user()->id)
        ->whereRaw('students_enrolled.id='.$id)
        //->join('users_students', 'users_students.id','=','students_enrolled.student_id')
        //->join('students_information', 'students_information.id','=','users_students.student_id')
        //->select('*')
        //->selectSub('students_enrolled.id', 'enrolled_id')
        //->selectSub('students_information.id', 'student_id')
        ->first();

        $this->template_data->set('enrolled', $enrolled );

        $school_fees = School_fees::where('school_year' , config('enrollment.current_school_year') )
        ->where('grade_level' , $enrolled->grade_level);

        $this->template_data->set('total_fees', $school_fees->sum('amount') );

        $fees = [];
        foreach($school_fees->get() as $item) {
            $fees[$item->month] = [ 'amount' => $item->amount, 'year' => $item->year ];
        }
        $this->template_data->set('school_fees', $fees );

        return view('user.enrollment.school_fees')->with( $this->template_data->get_data() );
    }

    /**
     * Show the application enrollment.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function payments($id)
    {
        $this->template_data->set('current_key', 'payments' );
        $this->template_data->set('enrolled_id', $id );

        $items = Users_payments::whereRaw( 'user_id='.auth()->user()->id )
        ->whereRaw( 'enrolled_id='.$id )
        ->paginate(10);
        $this->template_data->set('items', $items );

        return view('user.enrollment.payments')->with( $this->template_data->get_data() );
    }

    /**
     * Show the application enrollment.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function add_payment($id)
    {
        $this->template_data->set('current_key', 'payments' );
        $this->template_data->set('enrolled_id', $id );
        return view('user.enrollment.add_payment')->with( $this->template_data->get_data() );
    }

    /**
     * Show the application enrollment.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store_payment(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            //'payment_type' => 'required|string',
            'payment_method' => 'required|string',
            'notes' => 'nullable|string',
            'reference_number' => 'required|string',
            'transaction_date' => 'required|date',
            'amount' => 'required|string',
        ]);

        if ($validator->fails()) {

            return redirect("/enrollment/{$id}/payments")->withErrors( $validator )->withInput();

        } else {

            $bank_code = '';
            switch($request->input('payment_method')) {
                case 'online_banking_bdo':
                    $bank_code = 'BDO';
                break;
                case 'online_banking_pnb':
                    $bank_code = 'PNB';
                break;
                case 'online_banking_lbp':
                    $bank_code = 'LBP';
                break;
            }

            $payment = new Users_payments;
            $payment->user_id = $request->user()->id;
            $payment->payment_type = 'downpayment';
            $payment->payment_method = $request->input('payment_method');
            $payment->notes = $request->input('notes');
            $payment->bank_code = $bank_code;
            $payment->reference_id = $request->input('reference_number');
            $payment->date_deposited = date("Y-m-d H:i:s", strtotime($request->input('transaction_date')));
            $payment->amount = str_replace( "," , "" , $request->input('amount') );
            $payment->enrolled_id = $id;
            
            if( $payment->save() ) {
                return redirect("/enrollment/{$id}/payments");
            }
            return redirect("/enrollment/{$id}/payments");

        }
    }

    /**
     * Show the application enrollment.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function destroy_payment(Request $request, $enrolled_id, $payment_id)
    {
        
        Users_payments::whereRaw( 'user_id=' . $request->user()->id )
        ->whereRaw( 'id=' . $payment_id )
        ->whereRaw( 'enrolled_id=' . $enrolled_id )
        ->whereRaw( 'status="pending"' )
        ->delete();

        return redirect("/enrollment/{$enrolled_id}/payments");
    }

    /**
     * Show the application enrollment.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $this->template_data->set('enrolled_id', $id );

        $student = Students_enrolled::whereRaw('students_enrolled.user_id='. auth()->user()->id)
        ->whereRaw('students_enrolled.id='.$id)
        ->join('users_students', 'users_students.id','=','students_enrolled.student_id')
        ->join('students_information', 'students_information.id','=','users_students.student_id')
        ->select('*')
        ->selectSub('students_enrolled.id', 'enrolled_id')
        ->selectSub('students_information.id', 'student_id')
        ->first();

        $this->template_data->set('student', $student );

        if( $student ) {
            
            $enrolled_residential = Students_address::find($student->residential);
            $this->template_data->set( 'enrolled_residential', $enrolled_residential );

            $enrolled_provincial = Students_address::find($student->provincial);
            $this->template_data->set( 'enrolled_provincial', $enrolled_provincial );

            $enrolled_academic = Students_academic::find( $student->academic );
            $this->template_data->set( 'enrolled_academic', $enrolled_academic );

            $enrolled_father = Students_parents::find( $student->father );
            $this->template_data->set( 'enrolled_father', $enrolled_father );

            $enrolled_mother = Students_parents::find( $student->mother );
            $this->template_data->set( 'enrolled_mother', $enrolled_mother );

            $enrolled_guardian = Students_parents::find( $student->guardian );
            $this->template_data->set( 'enrolled_guardian', $enrolled_guardian );

            $enrolled_parents_status = Students_enrolled_meta::whereRaw('meta_key="parents_status" AND enrolled_id='.$id)->first();
            $this->template_data->set( 'enrolled_parents_status', $enrolled_parents_status );

            $enrolled_parents_status_other = Students_enrolled_meta::whereRaw('meta_key="parents_status_other" AND enrolled_id='.$id)->first();
            $this->template_data->set( 'enrolled_parents_status_other', $enrolled_parents_status_other );

            $enrolled_living_condition = Students_enrolled_meta::whereRaw('meta_key="living_condition" AND enrolled_id='.$id)->first();
            $this->template_data->set( 'enrolled_living_condition', $enrolled_living_condition );

            $enrolled_medical_condition = Students_enrolled_meta::whereRaw('meta_key="medical_condition" AND enrolled_id='.$id)->first();
            $this->template_data->set( 'enrolled_medical_condition', $enrolled_medical_condition );

            $enrolled_medical_condition_other = Students_enrolled_meta::whereRaw('meta_key="medical_condition_other" AND enrolled_id='.$id)->first();
            $this->template_data->set( 'enrolled_medical_condition_other', $enrolled_medical_condition_other );

            $enrolled_reason = Students_enrolled_meta::whereRaw('meta_key="reason" AND enrolled_id='.$id)->first();
            $this->template_data->set( 'enrolled_reason', $enrolled_reason );

            $addresses = Students_address::whereRaw('student_id='.$student->student_id)->get();
            $this->template_data->set( 'addresses', $addresses );

            $this->template_data->set( 'academics', Students_academic::whereRaw( 'student_id='. $student->student_id )->get() );

            $this->template_data->set( 'fathers', Students_parents::whereRaw('relationship="father" AND student_id='.$student->student_id )->get() );
            $this->template_data->set( 'mothers', Students_parents::whereRaw('relationship="mother" AND student_id='.$student->student_id )->get() );
            $this->template_data->set( 'guardians', Students_parents::whereRaw('relationship="guardian" AND student_id='.$student->student_id )->get() );
        
        } else {

            abort(403, 'Unauthorized action.');

        }

        return view('user.enrollment.edit')->with( $this->template_data->get_data() );
    }

    /**
     * Show the application enrollment.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function print($id)
    {
        $student = Students_enrolled::whereRaw('students_enrolled.user_id='. auth()->user()->id)
        ->whereRaw('students_enrolled.id='.$id)
        ->join('users_students', 'users_students.id','=','students_enrolled.student_id')
        ->join('students_information', 'students_information.id','=','users_students.student_id')
        ->select('*')
        ->selectSub('students_enrolled.student_id', 'student_id')
        ->selectSub('students_enrolled.id', 'enrolled_id')
        ->first();

        $this->template_data->set('student', $student );

        $_age = date_diff(date_create($student->birthday), date_create('now'))->y;
        $this->template_data->set('age', $_age );

        $enrolled_residential = Students_address::find($student->residential);
        $this->template_data->set( 'enrolled_residential', $enrolled_residential );

        $enrolled_provincial = Students_address::find($student->provincial);
        $this->template_data->set( 'enrolled_provincial', $enrolled_provincial );

        $enrolled_academic = Students_academic::find( $student->academic );
        $this->template_data->set( 'enrolled_academic', $enrolled_academic );

        $enrolled_father = Students_parents::find( $student->father );
        $this->template_data->set( 'enrolled_father', $enrolled_father );

        $enrolled_mother = Students_parents::find( $student->mother );
        $this->template_data->set( 'enrolled_mother', $enrolled_mother );

        $enrolled_guardian = Students_parents::find( $student->guardian );
        $this->template_data->set( 'enrolled_guardian', $enrolled_guardian );
        
        $enrolled_parents_status = Students_enrolled_meta::whereRaw('meta_key="parents_status" AND enrolled_id='.$id)->first();
        $this->template_data->set( 'enrolled_parents_status', $enrolled_parents_status );

        $enrolled_parents_status_other = Students_enrolled_meta::whereRaw('meta_key="parents_status_other" AND enrolled_id='.$id)->first();
        $this->template_data->set( 'enrolled_parents_status_other', $enrolled_parents_status_other );

        $enrolled_living_condition = Students_enrolled_meta::whereRaw('meta_key="living_condition" AND enrolled_id='.$id)->first();
        $this->template_data->set( 'enrolled_living_condition', $enrolled_living_condition );

        $enrolled_medical_condition = Students_enrolled_meta::whereRaw('meta_key="medical_condition" AND enrolled_id='.$id)->first();
        $this->template_data->set( 'enrolled_medical_condition', $enrolled_medical_condition );

        $enrolled_medical_condition_other = Students_enrolled_meta::whereRaw('meta_key="medical_condition_other" AND enrolled_id='.$id)->first();
        $this->template_data->set( 'enrolled_medical_condition_other', $enrolled_medical_condition_other );

        $enrolled_reason = Students_enrolled_meta::whereRaw('meta_key="reason" AND enrolled_id='.$id)->first();
        $this->template_data->set( 'enrolled_reason', $enrolled_reason );

        return view('user.enrollment.print_form')->with( $this->template_data->get_data() );
    }

    /**
     * Show the application enrollment.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update_enroll($id, $field, $value)
    {
        /*
            $enroll = Students_enrolled::find($id);
            if( $enroll->exists() ) {
                $enroll->$field = $value;
                $enroll->save();
            }
        */
            DB::table('students_enrolled')
            ->whereRaw( 'id='.$id )
            ->whereRaw( 'user_id='.auth()->user()->id )
            ->update([$field => $value]);

            return redirect("/enrollment/{$id}/edit");

    }

    /**
     * Show the application enrollment.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update_family(Request $request, $id)
    {
            foreach(['parents_status','parents_status_other','living_condition'] as $key) {
                $meta = Students_enrolled_meta::whereRaw('meta_key="'.$key.'" AND enrolled_id='.$id);
                if( $meta->exists() ) {
                    $meta->update( [ 'meta_value' => $request->input($key) ] );
                } else {
                    $new = new Students_enrolled_meta;
                    $new->enrolled_id = $id;
                    $new->meta_key = $key;
                    $new->meta_value = $request->input($key);
                    $new->save();
                }
            }
            return redirect("/enrollment/{$id}/edit");

        
    }

    /**
     * Show the application enrollment.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update_medical(Request $request, $id)
    {
            foreach(['medical_condition','medical_condition_other'] as $key) {
                $meta = Students_enrolled_meta::whereRaw('meta_key="'.$key.'" AND enrolled_id='.$id);
                if( $meta->exists() ) {
                    $meta->update( [ 'meta_value' => $request->input($key) ] );
                } else {
                    $new = new Students_enrolled_meta;
                    $new->enrolled_id = $id;
                    $new->meta_key = $key;
                    $new->meta_value = $request->input($key);
                    $new->save();
                }
            }
            return redirect("/enrollment/{$id}/edit");

        
    }

    /**
     * Show the application enrollment.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update_reason(Request $request, $id)
    {
        
            $meta = Students_enrolled_meta::whereRaw('meta_key="reason" AND enrolled_id='.$id);
            if( $meta->exists() ) {
                $meta->update( ['meta_value' => $request->input('reason')] );
            } else {
                $new = new Students_enrolled_meta;
                $new->enrolled_id = $id;
                $new->meta_key = 'reason';
                $new->meta_value = $request->input('reason');
                $new->save();
            }
            
            return redirect("/enrollment/{$id}/edit");

        
    }

}
