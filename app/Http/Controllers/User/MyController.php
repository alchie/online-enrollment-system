<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use App\Users_settings;
use App\Users_payments;

class MyController extends BaseController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->template_data->set('current_controller', 'my');
    }

    /**
     * set_user_settings
     *
     * @return \Illuminate\Http\Response
     */

    public function set_user_settings() {

        if( ! session()->get('system-started') ) {
            $user = auth()->user();
            $users_settings = DB::table('users_settings')
            ->where([
                ['user_id', '=', $user->id],
                ['settings_module', '=', 'theme'],
            ])->get();
            $theme = [];
            foreach($users_settings as $setting) {
                $theme[] = $setting->settings_key;
            } 
            session(['theme'=>$theme]);

            $this->_set_setting('theme','navbar', 'navbar-white navbar-light');
            $this->_set_setting('theme','accent', '');
            $this->_set_setting('theme','sidebar', 'sidebar-dark-primary');
            $this->_set_setting('theme','brand-logo', '');
            $this->_set_setting('theme','bg', 'bg-primary');

            session(['system-started'=>true]);
        }
        
        if( ! session()->get('user_permissions') ) {
            $user = auth()->user();
            $user_permissions = [];
            $from_db = Users_settings::where([['user_id', '=', $user->id],['settings_module','=',"user_permissions"]])->get();
            foreach($from_db as $db) {
                $user_permissions[$db->settings_key] = $db->settings_value;
            }
            session(['user_permissions'=>$user_permissions]);
        }

        return redirect('/dashboard');
    }

    /**
     * Save settings
     *
     * @return string
     */
    private function _set_setting($module, $key, $default='')
    {
        $user = auth()->user();
        
        $users_settings = DB::table('users_settings')
        ->where([
            ['user_id', '=', $user->id],
            ['settings_module', '=', 'theme'],
            ['settings_key', '=', $key],
        ])->first();

        if( $users_settings ) {
            session([ $module . '-' . $key => $users_settings->settings_value ]);
        } else {
            session([ $module . '-' . $key => $default ]);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        return view('user.my.profile')->with( $this->template_data->get_data() );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function settings()
    {
        $this->template_data->set('no_navbar_border', $this->settings_value('theme', 'no-navbar-border'));
        $this->template_data->set('navbar_small_text', $this->settings_value('theme', 'navbar-small-text'));
        $this->template_data->set('navbar_fixed', $this->settings_value('theme', 'navbar-fixed'));
        $this->template_data->set('body_small_text', $this->settings_value('theme', 'body-small-text'));
        $this->template_data->set('brand_small_text', $this->settings_value('theme', 'brand-small-text'));
        $this->template_data->set('sidebar_nav_small_text', $this->settings_value('theme', 'sidebar-nav-small-text'));
        $this->template_data->set('sidebar_collapsed', $this->settings_value('theme', 'sidebar-collapsed'));
        $this->template_data->set('sidebar_nav_flat_style', $this->settings_value('theme', 'sidebar-nav-flat-style'));
        $this->template_data->set('sidebar_nav_legacy_style', $this->settings_value('theme', 'sidebar-nav-legacy-style'));
        $this->template_data->set('sidebar_nav_compact', $this->settings_value('theme', 'sidebar-nav-compact'));
        $this->template_data->set('sidebar_nav_child_indent', $this->settings_value('theme', 'sidebar-nav-child-indent'));
        $this->template_data->set('main_sidebar_disable', $this->settings_value('theme', 'main-sidebar-disable'));
        $this->template_data->set('footer_small_text', $this->settings_value('theme', 'footer-small-text'));
        $this->template_data->set('footer_fixed', $this->settings_value('theme', 'footer-fixed'));
        return view('user.my.settings')->with( $this->template_data->get_data() );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save_settings(Request $request)
    {
        if( $request->input('theme') ) {
            $this->reset_settings('theme');
            foreach($request->input('theme') as $theme) {
                $this->store_settings('theme', $theme, 1);
            }
            session(['theme'=>$request->input('theme')]);
        } else {
            $this->reset_settings('theme');
            session(['theme'=>[]]);
        }

        $back = ( $request->input('back') ) ? $request->input('back') : '/my/settings';
        return redirect( $back );
    }

    public function change_color($key, $value)
    {
        switch($key)
        {
            case 'navbar-dark':

                $this->reset_settings_key('theme', 'bg');
                $this->store_settings('theme', 'bg', 'bg-'.$value);
                session(['theme-' . 'bg' => 'bg-'.$value]);

                $key = 'navbar';
                $value = 'navbar-dark navbar-' . $value;

            break;
            case 'navbar-light':

                $this->reset_settings_key('theme', 'bg');
                $this->store_settings('theme', 'bg', 'bg-'.$value);
                session(['theme-' . 'bg' => 'bg-'.$value]);

                $key = 'navbar';
                $value = 'navbar-light navbar-'. $value;
                
            break;
            case 'accent':
                $value = 'accent-'. $value;
            break;
            case 'dark-sidebar':
                $key = 'sidebar';
                $value = 'sidebar-dark-'. $value;
            break;
            case 'light-sidebar':
                $key = 'sidebar';
                $value = 'sidebar-light-'.$value;
            break;
            case 'brand-logo':
                $value = 'navbar-' . $value;
            break;
        }

        $this->reset_settings_key('theme', $key);
        $this->store_settings('theme', $key, $value);
        session(['theme-' . $key => $value]);

        return redirect( url('/my/settings') . "?back=" . request('back') );
    }

    public function reset_theme()
    {
        $user = auth()->user();

        DB::table('users_settings')
        ->where([
            ['user_id', '=', $user->id],
            ['settings_module', '=', 'theme'],
        ])->delete();
        
        session(['theme' => []]);
        session(['theme-navbar' => 'navbar-white navbar-light']);
        session(['theme-accent' => '']);
        session(['theme-sidebar' => 'sidebar-dark-primary']);
        session(['theme-brand-logo' => '']);
        session(['theme-bg' => 'bg-primary']);

        return redirect( url('/my/settings') . "?back=" . request('back') );
    }

    private function store_settings($module, $key, $value)
    {
        $user = auth()->user();

        $user_settings = new Users_settings;
        $user_settings->user_id = $user->id;
        $user_settings->settings_module = $module;
        $user_settings->settings_key = $key;
        $user_settings->settings_value = $value;
        $user_settings->save();

    }

    private function update_settings($module, $key, $value)
    {
        $user = auth()->user();

        DB::table('users_settings')
        ->where([
            ['user_id', '=', $user->id],
            ['settings_module', '=', $module],
            ['settings_key', '=', $key],
        ])
        ->update(
            ['settings_value' => $value]
        );
    }

    private function get_settings($module, $key)
    {
        $user = auth()->user();

        return DB::table('users_settings')
        ->where([
            ['user_id', '=', $user->id],
            ['settings_module', '=', $module],
            ['settings_key', '=', $key],
        ])->first();

    }

    private function settings_set($module, $key)
    {
        $user = auth()->user();

        return DB::table('users_settings')
        ->where([
            ['user_id', '=', $user->id],
            ['settings_module', '=', $module],
            ['settings_key', '=', $key],
        ])->count();

    }

    private function settings_value($module, $key)
    {
        $user = auth()->user();

        return DB::table('users_settings')
        ->where([
            ['user_id', '=', $user->id],
            ['settings_module', '=', $module],
            ['settings_key', '=', $key],
        ])->value('settings_value');

    }

    private function reset_settings($module)
    {
        $user = auth()->user();

        DB::table('users_settings')
        ->where([
            ['user_id', '=', $user->id],
            ['settings_module', '=', $module],
        ])->delete();

    }

    private function reset_settings_key($module, $key)
    {
        $user = auth()->user();

        DB::table('users_settings')
        ->where([
            ['user_id', '=', $user->id],
            ['settings_module', '=', $module],
            ['settings_key', '=', $key],
        ])->delete();

    }

    /**
     * Display a payments list.
     *
     * @return \Illuminate\Http\Response
     */
    public function payments()
    {
        $this->template_data->set('items', Users_payments::orderByDesc('date_deposited')->paginate(10) );
        return view('user.my.payments')->with( $this->template_data->get_data() );
    }



}
