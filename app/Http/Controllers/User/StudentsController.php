<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use App\Students_information;
use App\Claim_codes;
use App\Users_student;
use App\Students_address;
use App\Students_academic;

class StudentsController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->template_data->set('current_controller', 'students');
    }

    /**
     * Show the application students.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $students = Users_student::whereRaw('user_id='. auth()->user()->id)
        //->join('students_information', 'students_information.id','=','users_students.student_id')
        ->paginate(9);
        $this->template_data->set('students', $students );
        return view('user.students.index')->with( $this->template_data->get_data() );
    }

    /**
     * Show the application students.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        return view('user.students.create')->with( $this->template_data->get_data() );
    }

    /**
     * Show the application students.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function claim()
    {
        $this->template_data->set( 'student', false );
        $code = request()->query('code');
        if( $code ) {
            $claim_codes = Claim_codes::where([['code','=',$code]])
            ->whereRaw('((SELECT COUNT(*) FROM users_students WHERE users_students.student_id=claim_codes.student_id AND users_students.user_id='.auth()->user()->id.') = 0)')
            ->first();
            if( $claim_codes ) {
                $this->template_data->set( 'student', Students_information::find( $claim_codes->student_id ) );
            }
        }
        return view('user.students.claim')->with( $this->template_data->get_data() );
    }

    /**
     * Show the application students.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function check_claim(Request $request)
    {
        $claim_codes = Claim_codes::where([['code','=',$request->query('code')]]);
        if( $claim_codes->first() ) {
            $student = new Users_student;
            $student->user_id = $request->user()->id;
            $student->student_id = $claim_codes->first()->student_id;
            if( $student->save() ) {
                $claim_codes->delete();
                return redirect("/students");
            }
        }
        return redirect("/students/claim");
    }

    /**
     * Show the application students.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function personal_information( $id )
    {
        
        $this->template_data->set( 'current_key', 'personal' );
        $this->template_data->set( 'student', Students_information::find($id) );
        return view('user.students.personal_information')->with( $this->template_data->get_data() );
    }

    /**
     * Show the application enrollment.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update_personal(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'last_name' => 'required|string|max:255',
            'first_name' => 'required|string|max:255',
            'middle_name' => 'required|string|max:255',
            //'birth_date' => 'required|string|max:255',
            //'birth_place' => 'required|string|max:255',
            //'religion' => 'required|string|max:255',
            //'gender' => 'required|string|max:1',
        ]);

        if ($validator->fails()) {

            return redirect("/students/{$id}/personal")->withErrors( $validator )->withInput();

        } else {

            $info = Students_information::find($id);
            $info->lastname = $request->input('last_name');
            $info->firstname = $request->input('first_name');
            $info->middlename = $request->input('middle_name');
            $info->birthday = date( "Y-m-d", strtotime($request->input('birth_date')) );
            $info->birthplace = $request->input('birth_place');
            $info->religion = $request->input('religion');
            $info->gender = $request->input('gender');
            $info->save();

            return redirect("/students/{$id}/personal");

        }
    }


}
