<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\User;

class PagesController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->template_data->set('current_controller', 'welcome');
    }

    /**
     * Show the application payments.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function welcome()
    {
        $this->template_data->set('current_controller', 'welcome');
        return view('user.pages.welcome')->with( $this->template_data->get_data() );
    }

}
