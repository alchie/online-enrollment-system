<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;

class BaseController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware(['auth','settings.theme']);

        $this->template_data->set('search_form_show', false);
        $this->template_data->set('search_form_placeholder', 'Search');
        
    }

}
