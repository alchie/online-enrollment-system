<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\User;
use App\Users_payments;

class PaymentsController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->template_data->set('current_controller', 'payments');
    }

    /**
     * Show the application payments.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $items = Users_payments::whereRaw( 'user_id='.auth()->user()->id )
        ->paginate(10);
        $this->template_data->set('items', $items );

        return view('user.payments.index')->with( $this->template_data->get_data() );
    }

    /**
     * Show the application enrollment.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function destroy(Request $request, $payment_id)
    {
        
        Users_payments::whereRaw( 'user_id=' . $request->user()->id )
        ->whereRaw( 'id=' . $payment_id )
        ->whereRaw( 'status="pending"' )
        ->delete();

        return redirect("/payments");
    }

}
