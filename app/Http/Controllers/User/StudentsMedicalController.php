<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Students_address;
use App\Students_information;

class StudentsMedicalController extends BaseController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->template_data->set('current_controller', 'students');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( $id )
    {
        $this->template_data->set( 'student', Students_information::find($id) );
        return view('user.students.medical_information')->with( $this->template_data->get_data() );
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_address(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'address' => 'required|string',
        ]);

        if ($validator->fails()) {

            return redirect("/students/{$id}/address_contacts")->withErrors( $validator )->withInput();

        } else {

            $info = new Students_address;
            $info->student_id = $id;
            $info->address = $request->input('address');
            $info->zip = $request->input('zip');
            $info->save();

            return redirect("/students/{$id}/address_contacts");

        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Students_academic  $students_academic
     * @return \Illuminate\Http\Response
     */
    public function edit( $id )
    {
        $this->template_data->set( 'address', Students_address::find($id) );
        return view('user.students.edit_address')->with( $this->template_data->get_data() );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Students_academic  $students_academic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'address' => 'required|string',
        ]);

        if ($validator->fails()) {

            return redirect("/students/{$id}/edit_address")->withErrors( $validator )->withInput();

        } else {

            $info = Students_address::find($id);
            $info->address = $request->input('address');
            $info->zip = $request->input('zip');
            $info->save();

            return redirect("/students/{$info->student_id}/address_contacts");

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Students_academic  $students_academic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Students_academic $students_academic)
    {
        //
    }
}
