<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Students_parents;
use App\Students_information;

class StudentsParentsController extends BaseController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->template_data->set('current_controller', 'students');
        $this->template_data->set( 'current_key', 'family' );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( $id )
    {
       
        $this->template_data->set( 'student', Students_information::find($id) );

        return view('user.students.family_information')->with( $this->template_data->get_data() );
   }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'full_name' => 'required|string',
            'relationship' => 'required|string',
            'occupation' => 'required|string',
            'phone' => 'required|string',
        ]);

        if ($validator->fails()) {

            return redirect("/students/{$id}/family")->withErrors( $validator )->withInput();

        } else {

            $info = new Students_parents;
            $info->student_id = $id;
            $info->name = $request->input('full_name');
            $info->relationship = $request->input('relationship');
            $info->phone = $request->input('phone');
            $info->occupation = $request->input('occupation');
            $info->save();

            return redirect("/students/{$id}/family");

        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Students_academic  $students_academic
     * @return \Illuminate\Http\Response
     */
    public function edit( $id )
    {
        $parent = Students_parents::find($id);
        $this->template_data->set( 'student_id', $parent->student_id );
        $this->template_data->set( 'parent', $parent );
        return view('user.students.edit_parent')->with( $this->template_data->get_data() );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Students_academic  $students_academic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'full_name' => 'required|string',
            'relationship' => 'required|string',
            'occupation' => 'required|string',
            'phone' => 'required|string',
        ]);

        if ($validator->fails()) {

            return redirect("/students/{$id}/edit_parent")->withErrors( $validator )->withInput();

        } else {

            $info = Students_parents::find( $id );
            $info->name = $request->input('full_name');
            $info->relationship = $request->input('relationship');
            $info->phone = $request->input('phone');
            $info->occupation = $request->input('occupation');
            $info->save();

            return redirect("/students/{$info->student_id}/family");

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Students_academic  $students_academic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Students_academic $students_academic)
    {
        //
    }
}
