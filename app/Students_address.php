<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Students_address extends Model
{
    protected $table = 'students_address';

    public function information() {
        return $this->hasOne('App\Students_information', 'id', 'student_id');
    }
}
