<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Claim_codes extends Model
{
    public function information() {
        return $this->belongsTo('App\Students_information', 'id', 'student_id');
    }
}
