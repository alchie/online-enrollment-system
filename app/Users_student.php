<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users_student extends Model
{
    public function information() {
        return $this->hasOne('App\Students_information', 'id', 'student_id');
    }
}
