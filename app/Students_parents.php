<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Students_parents extends Model
{
    protected $table = 'students_parents';

    public function information() {
        return $this->hasOne('App\Students_information', 'id', 'student_id');
    }

}
