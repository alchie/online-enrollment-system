<?php 

return [

    'modules' => [
        'enrollees' => ['name' => 'Enrollees'],
        'payments' => ['name' => 'Payments'],
        'school_fees' => ['name' => 'School Fees'],
        'students' => ['name' => 'Students'],
        'users' => ['name' => 'Users'],
    ],

    'actions' => [
        'create' => ['name' => 'Create', 'prime' => '3'],
        'read' => ['name' => 'Read', 'prime' => '5'],
        'update' => ['name' => 'Update', 'prime' => '7'],
        'delete' => ['name' => 'Delete', 'prime' => '11'],
    ],

    'grade_levels' => [
        'K1' => ['name' => 'Kinder 1'],
        'K2' => ['name' => 'Kinder 2'],
        'G1' => ['name' => 'Grade 1'],
        'G2' => ['name' => 'Grade 2'],
        'G3' => ['name' => 'Grade 3'],
        'G4' => ['name' => 'Grade 4'],
        'G5' => ['name' => 'Grade 5'],
        'G6' => ['name' => 'Grade 6'],
        'G7' => ['name' => 'Grade 7'],
        'G8' => ['name' => 'Grade 8'],
        'G9' => ['name' => 'Grade 9'],
        'G10' => ['name' => 'Grade 10'],
        'G11' => ['name' => 'Grade 11'],
        'G12' => ['name' => 'Grade 12'],
    ],

    'school_years' => [
        'SY2021' => ['name' => 'SY 2020-2021'],
    ],
    
    'current_school_year' => 'SY2021',
    
    'payment_types' => [
        'downpayment' => ['name' => 'Downpayment'],
    ],

    'payment_methods' => [
        'online_banking_bdo' => ['name' => 'Online Banking - BDO'],
        'online_banking_bpi' => ['name' => 'Online Banking - BPI'],
        'online_banking_lbp' => ['name' => 'Online Banking - Landbank'],
    ],

];