<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * Auth Routes
 */
Auth::routes();

/* ------------------------------------------------------------------------------------
 * User Routes
 ------------------------------------------------------------------------------------ */
// PagesController
Route::get('/', 'User\PagesController@welcome');
Route::get('/welcome', 'User\PagesController@welcome');

// enrollment
Route::get('/enrollment', 'User\EnrollmentController@index');
Route::get('/enrollment/enroll', 'User\EnrollmentController@enroll');
Route::post('/enrollment/enroll', 'User\EnrollmentController@store_enroll');
Route::get('/enrollment/{id}/edit', 'User\EnrollmentController@edit');
Route::get('/enrollment/{id}/print', 'User\EnrollmentController@print');
Route::get('/enrollment/{id}/preview', 'User\EnrollmentController@preview');
Route::get('/enrollment/{id}/school_fees', 'User\EnrollmentController@school_fees');
Route::get('/enrollment/{id}/payments', 'User\EnrollmentController@payments');
Route::get('/enrollment/{id}/add_payment', 'User\EnrollmentController@add_payment');
Route::post('/enrollment/{id}/payments', 'User\EnrollmentController@store_payment');
Route::delete('/enrollment/{enrolled_id}/payments/{payment_id}', 'User\EnrollmentController@destroy_payment');

// updates
Route::get('/enrollment/{id}/update/{field}/{value}', 'User\EnrollmentController@update_enroll');
Route::post('/enrollment/{id}/update_family', 'User\EnrollmentController@update_family');
Route::post('/enrollment/{id}/update_medical', 'User\EnrollmentController@update_medical');
Route::post('/enrollment/{id}/update_reason', 'User\EnrollmentController@update_reason');

// students
Route::get('/students', 'User\StudentsController@index');
Route::get('/students/create', 'User\StudentsController@create');
Route::get('/students/claim', 'User\StudentsController@claim');
Route::post('/students/claim', 'User\StudentsController@check_claim');
Route::get('/students/{id}/personal', 'User\StudentsController@personal_information');
Route::post('/students/{id}/personal', 'User\StudentsController@update_personal');

Route::get('/students/{id}/address_contacts', 'User\StudentsAddressController@index');
Route::post('/students/{id}/address_contacts', 'User\StudentsAddressController@store');
Route::get('/students/{id}/edit_address', 'User\StudentsAddressController@edit');
Route::post('/students/{id}/edit_address', 'User\StudentsAddressController@update');

Route::get('/students/{id}/academic', 'User\StudentsAcademicController@index');
Route::post('/students/{id}/academic', 'User\StudentsAcademicController@store');
Route::get('/students/{id}/edit_academic', 'User\StudentsAcademicController@edit');
Route::post('/students/{id}/edit_academic', 'User\StudentsAcademicController@update');

Route::get('/students/{id}/family', 'User\StudentsParentsController@index');
Route::post('/students/{id}/family', 'User\StudentsParentsController@store');
Route::get('/students/{id}/edit_parent', 'User\StudentsParentsController@edit');
Route::post('/students/{id}/edit_parent', 'User\StudentsParentsController@update');

Route::get('/students/{id}/medical', 'User\StudentsMedicalController@index');

// payments
Route::get('/payments', 'User\PaymentsController@index');
Route::delete('/payments/{id}', 'User\PaymentsController@destroy');

// my
//Route::get('/my/profile', 'User\MyController@profile');
Route::get('/my/change-color/{key}/{value}', 'User\MyController@change_color');
Route::get('/my/settings', 'User\MyController@settings');
Route::get('/my/reset-theme', 'User\MyController@reset_theme');
Route::post('/my/settings', 'User\MyController@save_settings');
//Route::get('/my/payments', 'User\MyController@payments');

/* ------------------------------------------------------------------------------------
 * Manage Routes
 ------------------------------------------------------------------------------------ */
// dashboard
Route::get('/manage/dashboard', 'Manage\DashboardController@index');

// enrolled
Route::get('/manage/enrollees', 'Manage\EnrolleesController@index');

// payments
Route::resource('/manage/payments', 'Manage\PaymentsController');

// school_fees
Route::get('/manage/school_fees', 'Manage\SchoolFeesController@index');
Route::get('/manage/school_fees/{grade_level}/edit', 'Manage\SchoolFeesController@edit');
Route::post('/manage/school_fees/{grade_level}/edit', 'Manage\SchoolFeesController@update');

// students
Route::resource('/manage/students', 'Manage\StudentsInformationController')->except(['show']);
Route::get('/manage/students/generate_claim_codes', 'Manage\StudentsInformationController@generate_claim_codes');
Route::get('/manage/students/{id}/generate_claim_code', 'Manage\StudentsInformationController@generate_claim_code');
Route::get('/manage/students/{id}/claim_codes', 'Manage\StudentsInformationController@claim_codes');
Route::get('/manage/students/{id}/address_contacts', 'Manage\StudentsAddressController@index');
Route::get('/manage/students/{id}/academics', 'Manage\StudentsAcademicsController@index');
Route::get('/manage/students/{id}/family', 'Manage\StudentsFamilyController@index');

// user
Route::resource('/manage/users', 'Manage\UsersController');
Route::put('/manage/users/{id}/permissions', 'Manage\UsersController@permissions');
Route::put('/manage/users/{id}/save_admin', 'Manage\UsersController@save_admin');
Route::get('/manage/users/{id}/payments', 'Manage\UsersController@payments');
Route::get('/manage/users/{id}/edit_payment', 'Manage\UsersController@edit_payment');