@extends('manage.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit School Fee</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/manage/school_fees">School Fees</a></li>
              <li class="breadcrumb-item active">Edit School Fee</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">
<div class="container">

<div class="row">
<div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{ config('enrollment.grade_levels')[$grade_level]['name'] }} School Fees</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" action="">
              @csrf
                <div class="card-body">

<div class="row">
<div class="col-md-12">
                  <div class="form-group">
                    <label for="name">Downpayment</label>
                    <input value="{{ $fees['downpayment']['amount'] ?? '0.00' }}" type="text" class="form-control text-right" id="name" placeholder="Enter Downpayment" name="amount[downpayment]">
                  </div>
</div>
@foreach([
6,7,8,9,10,11,12,1,2,3
] as $month)
<div class="col-md-12">
<div class="row">
<div class="col-md-6">
                  <div class="form-group">
                    <label for="name">{{ date("F", strtotime($month."/1/2000")) }} Year</label>
                    <input value="{{ $fees[strtolower(date("F", strtotime($month."/1/2000")))]['year'] ?? date("Y") }}" type="text" class="form-control text-right" id="name" placeholder="{{ date("F", strtotime($month."/1/2000")) }} Year" name="year[{{ strtolower(date("F", strtotime($month."/1/2000"))) }}]">
                  </div>
</div>
<div class="col-md-6">
                  <div class="form-group">
                    <label for="name">{{ date("F", strtotime($month."/1/2000")) }} Amount</label>
                    <input value="{{ $fees[strtolower(date("F", strtotime($month."/1/2000")))]['amount'] ?? '0.00' }}" type="text" class="form-control text-right" id="name" placeholder="{{ date("F", strtotime($month."/1/2000")) }} Amount" name="amount[{{ strtolower(date("F", strtotime($month."/1/2000"))) }}]">
                  </div>
</div>
</div>
</div>
@endforeach

<div class="col-md-12">
                  <div class="form-group">
                    <label for="name">Total Fees</label>
                  <div class="form-control text-right"><strong>{{ number_format($total_fees,2) }}</strong></div>
                  </div>
</div>

</div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

          </div>
</div>

</div>
</section>
@endsection
