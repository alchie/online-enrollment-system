@extends('manage.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>School Fees</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/manage/dashboard">Home</a></li>
              <li class="breadcrumb-item active">School Fees</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">
<div class="container-fluid">

<div class="row">

@foreach(config('enrollment.grade_levels') as $key => $grade_level ) 
<div class="col-md-4">
            <!-- Widget: user widget style 2 -->
            <div class="card card-widget widget-user-2">
            
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header bg-default">
              <a href="/manage/school_fees/{{ $key }}/edit" class="btn btn-warning btn-xs float-right"><i class="fa fa-pencil"></i></a>
                
                <!-- /.widget-user-image -->
                <h3 class="widget-user-username">{{ $grade_level['name'] }}</h3>
                <h5 class="widget-user-desc">{{ (isset($total_fees[$key])) ? number_format($total_fees[$key],2) : '0.00' }}</h5>
                <span class="badge badge-primary float-right"></span>
              </div>

            </div>
            <!-- /.widget-user -->
          </div>
          <!-- /.col -->
@endforeach


</div>

</div>
</section>
@endsection
