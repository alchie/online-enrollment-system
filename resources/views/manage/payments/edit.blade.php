@extends('manage.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Payments
            
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/manage/payments">Payments</a></li>
              <li class="breadcrumb-item active">Edit Payment</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">
<div class="container">

<div class="row">
<div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Payment Details</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
                <div class="card-body">
<div class="row">
<div class="col-md-6">
                  <div class="form-group">
                    <label for="name">Payment Type</label>
                    <div class="form-control">{{ config('enrollment.payment_types')[$current_item->payment_type]['name'] }}</div>
                  </div>
                  <div class="form-group">
                    <label for="name">Payment Method</label>
                    <div class="form-control">{{ config('enrollment.payment_methods')[$current_item->payment_method]['name'] }}</div>
                  </div>
                  <div class="form-group">
                    <label for="name">Notes / Remarks</label>
                    <div class="form-control">{{ $current_item->notes }}</div>
                  </div>
                  
</div>
<div class="col-md-6">
                  <div class="form-group">
                    <label for="name">Reference / Transaction Number</label>
                    <div class="form-control">{{ $current_item->reference_id }}</div>
                  </div>
                  <div class="form-group">
                    <label for="name">Transaction Date</label>
                    <div class="form-control">{{ date("F d, Y H:i:s", strtotime($current_item->date_deposited)) }}</div>
                  </div>
                  <div class="form-group">
                    <label for="name">Amount</label>
                    <div class="form-control text-right">{{ number_format($current_item->amount,2) }}</div>
                  </div>
</div>
</div>

                </div>
                <!-- /.card-body -->

                
            </div>
            <!-- /.card -->

<div class="row">
<div class="col-6">

            <div class="card card-warning">
              <div class="card-header">
                <h3 class="card-title">Edit Payment</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" action="/manage/payments/{{$current_item->id}}">
              @csrf
              @method('PUT')
                <div class="card-body">

                <div class="form-group">
                    <label for="name">Status</label>
                    <select name="status" class="form-control">
@foreach([
'pending' => 'Pending',
'verified' => 'Verified'
] as $key => $value)
                      <option {{ ($key==$current_item->status) ? 'SELECTED' : ''}} value="{{ $key }}">{{ $value }}</option>
@endforeach
                    </select>
                  </div>

                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update Status</button>
                </div>
              </form>
              </div>
              </div>
</div>

          </div>
</div>

</div>
</section>
@endsection
