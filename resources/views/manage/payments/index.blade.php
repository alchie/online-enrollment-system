@extends('manage.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Payments</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/manage/dashboard">Home</a></li>
              <li class="breadcrumb-item active">Payments</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">
<div class="container-fluid">

<div class="row">
          <div class="col-12">
            <div class="card">
@if( ($items) && $items->isNotEmpty() )
<!--
              <div class="card-header">
                <h3 class="card-title">Students List</h3>

                <div class="card-tools">
                </div>
              </div>
              <#!-- /.card-header -->
              <div class="card-body table-responsive p-0">

                <table class="table table-hover table-striped text-nowrap">
                  <thead>
                    <tr>
                      <th>Reference ID</th>
                      <th>Transaction Date</th>
                      <th>Amount</th>
                      <th>Status</th>
                      <th class="text-right" width="50px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
@foreach($items as $item)
                    <tr>
                      <td>{{$item->reference_id}}</td>
                      <td>{{ date( "m/d/Y H:i:s", strtotime($item->date_deposited) ) }}</td>
                      <td>{{ number_format($item->amount,2) }}</td>
                      <td><span class="badge badge-{{ ($item->status=='pending') ? 'warning' : 'success' }}">{{ ucwords($item->status) }}</span></td>
                      <td class="text-right">
                      <a href="/manage/payments/{{$item->id}}/edit" class="btn btn-warning btn-xs">Edit</a>
                      </td>
                    </tr>
@endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->

@if( $items->hasPages() )
              <div class="card-footer">{{$items->links()}}</div>
@endif

@else
<div class="card-body text-center">Nothing found!</div>
@endif
            </div>
            <!-- /.card -->
          </div>
</div>

</div>
</section>
@endsection
