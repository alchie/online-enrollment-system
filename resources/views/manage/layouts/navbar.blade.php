  <nav class="main-header navbar navbar-expand <?php echo session()->get('theme-navbar'); ?> <?php echo (in_array('no-navbar-border', session()->get('theme'))) ? "border-bottom-0" : ''; ?> <?php echo (in_array('navbar-small-text', session()->get('theme'))) ? "text-sm" : ''; ?>">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/manage/dashboard" class="nav-link">Home</a>
      </li>
      <!--<li class="nav-item d-none d-sm-inline-block">
        <a href="/" class="nav-link">Contact</a>
      </li>-->
    </ul>

@if($search_form_show)
    <!-- SEARCH FORM -->
    <form class="form-inline ml-3" method="GET">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" name="q" value="{{ request()->query('q') }}" type="search" placeholder="{{ $search_form_placeholder }}" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
    </form>
@endif

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">

    <li class="nav-item dropdown">
        <a id="dropdown-user-menu" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"><i
              class="fa fa-user"></i>  {{ Auth::user()->name }} </a>
            <ul aria-labelledby="dropdown-user-menu" class="dropdown-menu-right dropdown-menu border-0 shadow">
              
              <li><a href="/my/settings?back={{ url()->current() }}" class="dropdown-item">My Settings</a></li>

              @if( auth()->user()->is_admin )
              <li class="dropdown-divider"></li>
              <li><a href="/" class="dropdown-item">User Account</a></li>
              @endif
              <li class="dropdown-divider"></li>
              <li>
              <a class="dropdown-item" href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                  {{ __('Logout') }}
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
              </form>
              </li>
            </ul>
          
        </li>
    
    </ul>
  </nav>
 