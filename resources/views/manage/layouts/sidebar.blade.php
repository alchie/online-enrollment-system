<aside class="main-sidebar <?php echo (session()->get('theme-sidebar')) ? session()->get('theme-sidebar') : 'sidebar-dark-primary'; ?> elevation-4 <?php echo (in_array('main-sidebar-disable', session()->get('theme'))) ? "sidebar-no-expand" : ''; ?>">
    <!-- Brand Logo -->
    <div class="brand-link <?php echo session()->get('theme-brand-logo'); ?> <?php echo (in_array('brand-small-text', session()->get('theme'))) ? "text-sm" : ''; ?>">
      <img src="/storage/img/AdminLTELogo.png"
           alt="{{env('APP_NAME')}}"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">{{env('APP_NAME')}}</span>
    </div>

    <!-- Sidebar -->
    <div class="sidebar">

<?php 
$sidebar_class = '';
$sidebar_class .= (in_array('sidebar-nav-small-text', session()->get('theme'))) ? "text-sm " : '';
$sidebar_class .= (in_array('sidebar-nav-flat-style', session()->get('theme'))) ? "nav-flat " : '';
$sidebar_class .= (in_array('sidebar-nav-legacy-style', session()->get('theme'))) ? "nav-legacy " : ''; 
$sidebar_class .= (in_array('sidebar-nav-compact', session()->get('theme'))) ? "nav-compact " : '';
$sidebar_class .= (in_array('sidebar-nav-child-indent', session()->get('theme'))) ? "nav-child-indent " : ''; 
?>
    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column {{ $sidebar_class }}" data-widget="treeview" role="menu" data-accordion="false">
        
        <li class="nav-item">
            <a href="/manage/dashboard" class="nav-link @if('dashboard'==$current_controller) active @endif">
              <i class="nav-icon fa fa-dashboard"></i>
              <p>Dashboard</p>
            </a>
          </li>
@foreach([
  ['key' => 'enrollees', 'name' => 'Enrollees', 'url' => '/manage/enrollees', 'icon' => 'fa fa-id-card-o'],
  ['key' => 'payments', 'name' => 'Payments', 'url' => '/manage/payments', 'icon' => 'fa fa-money'],
  ['key' => 'school_fees', 'name' => 'School Fees', 'url' => '/manage/school_fees', 'icon' => 'fa fa-dollar'],
  ['key' => 'students', 'name' => 'Students', 'url' => '/manage/students', 'icon' => 'fa fa-user'],
  ['key' => 'users', 'name' => 'Users', 'url' => '/manage/users', 'icon' => 'fa fa-users'],
] as $nav)

@if($nav['key'] == '--header--')
<li class="nav-header">{{ $nav['name'] }}</li>
@else

@if( ( isset( session()->get('user_permissions')[$nav['key']] ) ) && ( session()->get('user_permissions')[$nav['key']] % 5 ) == 0 )
          <li class="nav-item">
            <a href="{{ $nav['url'] }}" class="nav-link @if($nav['key']==$current_controller) active @endif">
              <i class="nav-icon {{ $nav['icon'] }}"></i>
              <p>{{ $nav['name'] }}</p>
            </a>
          </li>
@endif


@endif
@endforeach

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
      
    </div>
    <!-- /.sidebar -->
  </aside>