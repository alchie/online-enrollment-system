@extends('manage.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Enrollees</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/manage/dashboard">Home</a></li>
              <li class="breadcrumb-item active">Enrollees</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">
<div class="container-fluid">

<div class="row">
          <div class="col-12">
            <div class="card">
@if( ($items) && $items->isNotEmpty() )
<!--
              <div class="card-header">
                <h3 class="card-title">Students List</h3>

                <div class="card-tools">
                </div>
              </div>
              <#!-- /.card-header -->
              <div class="card-body table-responsive p-0">

                <table class="table table-hover table-striped text-nowrap">
                  <thead>
                    <tr>
                      
                      <th>Last Name</th>
                      <th>First Name</th>
                      <th>Grade Level</th>
                      <th class="text-right">Action</th>
                    </tr>
                  </thead>
                  <tbody>
@foreach($items as $item)
                    <tr>
                      
                      <td>{{$item->lastname}}</td>
                      <td>{{$item->firstname}}</td>
                      <td><span class="badge badge-success">{{ config('enrollment.grade_levels')[$item->grade_level]['name'] }}</span></td>
                      <td class="text-right">
                      <a href="/manage/enrollees/{{$item->id}}/edit" class="btn btn-warning btn-xs">Edit</a>
                      <a class="btn btn-danger btn-xs confirm_delete" href="javascript:void(0);" data-id="delete-{{$item->id}}">Delete</a>
                      <form id="delete-{{$item->id}}" action="/manage/enrollees/{{ $item->id }}" method="POST" style="display: none;">@csrf @method('DELETE')</form>
                      </td>
                    </tr>
@endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->

@if( $items->hasPages() )
              <div class="card-footer">{{$items->links()}}</div>
@endif

@else
<div class="card-body text-center">Nothing found!</div>
@endif
            </div>
            <!-- /.card -->
          </div>
</div>

</div>
</section>
@endsection
