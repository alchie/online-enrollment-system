@extends('manage.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>User Payment</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/manage/users">Users</a></li>
              <li class="breadcrumb-item"><a href="/manage/users/{{$payment->created_by}}/payments">Payments</a></li>
              <li class="breadcrumb-item active">Edit Payment</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">

<div class="container-fluid">
<div class="row">
<div class="col-6">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Payment</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="/manage/users/{{ $payment->id }}/change_status">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="status">Status</label>
                    <select name="status" class="form-control" id="status">
                    @foreach(config('mlm.payment_status') as $key => $status)
                        <option @if($key==$payment->status) SELECTED @endif value="{{ $key }}">{{ $status['name'] }}</option>
                    @endforeach
                    </select>
                  </div>
                  

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
</div>

@if( $payment->status == 'verified' )
<div class="col-6">
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">Add Receipt</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="/manage/users/{{ $payment->id }}/add_receipt">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="status">Receipt Number</label>
                    <input type="text" class="form-control text-center" name="value">
                  </div>
                  

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
</div>
@endif

@if( $payment->status == 'verified' )
@if( $payment->payment_type == 'account_upgrade' )
<div class="col-6">
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">Premium Account</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="/manage/users/{{ $payment->id }}/upgrade_account1">
                @csrf

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Upgrade Now</button>
                </div>
              </form>
            </div>
</div>
@elseif( $payment->payment_type == 'buy_tokens' )
<div class="col-12">
            <div class="card card-default">
              <div class="card-header">
@if( $tokens->count() < 100 )
              <form method="POST" action="/manage/users/{{$payment->id}}/generate_tokens">
                @csrf
                <button type="submit" class="float-right btn btn-warning btn-xs">Generate 10 Tokens</button>
                </form>
@endif
                <h3 class="card-title">Reservation Tokens <span class="badge badge-primary">{{ $tokens->count() }} Tokens</span></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <div class="card-body p-0">
@if( $tokens->count() > 0 )
                <table class="table table-sm">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Token</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Phone Number</th>
                    </tr>
                  </thead>
                  <tbody>
@php $i = 1; @endphp
@foreach($tokens as $token)
                    <tr>
                      <td>{{ $i++ }}</td>
                      <td>{{ substr($token->token, 0, 10) }}...</td>
                      <td>{{ $token->name }}</td>
                      <td>{{ $token->email }}</td>
                      <td>{{ $token->phone_number }}</td>
                    </tr>
@endforeach
                  </tbody>
                </table>
@else
<p class="text-center">No tokens generated!</p>
@endif
              </div>

                <!-- /.card-body -->


            </div>
</div>
@endif
@endif

</div>
</div>

</section>

@endsection
