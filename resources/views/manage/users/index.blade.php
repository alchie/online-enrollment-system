@extends('manage.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Users
            <a href="/manage/users/create" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add User</a>
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item active">Users</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">

<div class="container-fluid">

<div class="row">
          <div class="col-12">
<div class="card">
@if(($items) && $items->isNotEmpty())
<!--
          <div class="card-header">
          
            <h3 class="card-title">Users List</h3>
            <div class="card-tools">
            
            </div>
          </div>
          <#!-- /.card-header -->
          <div class="card-body p-0">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Date Created</th>
                  <th style="width: 150px">Action</th>
                </tr>
              </thead>
              <tbody>
@foreach($items as $item)
                <tr>
                  <td>{{ $item->name }}</td>
                  <td>{{ $item->email }}</td>
                  <td>{{ date('F d, Y H:i:s', strtotime($item->created_at)) }}</td>
                  <td>

<!-- Split button -->
<div class="btn-group">
  <a class="btn btn-warning btn-xs" href="/manage/users/{{ $item->id }}/edit">Edit</a>  
  <button type="button" class="btn btn-warning btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu dropdown-menu-right">
    
    <li><a class="dropdown-item" href="/manage/users/{{ $item->id }}/payments">Payments</a></li>
    
  </ul>
</div>


                    <a class="btn btn-danger btn-xs confirm_delete" href="javascript:void(0);" data-id="delete-{{$item->id}}">Delete</a>
                    <form id="delete-{{$item->id}}" action="/manage/users/{{ $item->id }}" method="POST" style="display: none;">@csrf @method('DELETE')</form>
                </td>
                </tr>
@endforeach
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
@if( $items->hasPages() )
              <div class="card-footer">{{$items->links()}}</div>
@endif

@else
<div class="card-body text-center">Nothing found!</div>
@endif
        </div>

</div>
</div>
</div>

</section>

@endsection
