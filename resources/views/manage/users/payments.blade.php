@extends('manage.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Users</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/manage/users">Users</a></li>
              <li class="breadcrumb-item active">Payments</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
<div class="container-fluid">

<div class="row">
          <div class="col-12">
            <div class="card">
@if( ($items) && $items->isNotEmpty() )
<!--
              <div class="card-header">
                <h3 class="card-title">Withdrawals List</h3>

                <div class="card-tools">
                </div>
              </div>
              <#!-- /.card-header -->
              <div class="card-body table-responsive p-0">

                <table class="table table-hover table-striped text-nowrap">
                  <thead>
                    <tr>
                      <th>Type</th>
                      <th>Method</th>
                      <th>Reference ID</th>
                      <th>Payment Date</th>
                      <th class="text-right">Amount</th>
                      <th class="text-right">Status</th>
                      <th class="text-right">Action</th>
                    </tr>
                  </thead>
                  <tbody>
@foreach($items as $item)
                    <tr>
                      <td>{{ config('mlm.payment_types')[$item->payment_type]['name'] }}</td>
                      <td>{{ config('mlm.payment_methods')[$item->payment_method]['name']}}</td>
                      <td>{{$item->reference_id}}</td>
                      <td>{{ date("F d, Y H:i:s", strtotime($item->date_deposited)) }}</td>
                      <td class="text-right">{{ number_format($item->amount,2) }}</td>
                      <td class="text-right"><span class="badge badge-{{ config('mlm.payment_status')[$item->status]['color'] }}">{{ config('mlm.payment_status')[$item->status]['name'] }}</span></td>
                      <td class="text-right">
                        <a href="/manage/users/{{$item->id}}/edit_payment" class="btn btn-warning btn-xs">Edit</a>
                      </td>
                    </tr>
@endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->

@if( $items->hasPages() )
              <div class="card-footer">{{$items->links()}}</div>
@endif

@else
<div class="card-body text-center">Nothing found!</div>
@endif
            </div>
            <!-- /.card -->
          </div>
</div>

</div>
</section>

@endsection
