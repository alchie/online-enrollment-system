@extends('manage.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Users</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/manage/users">Users</a></li>
              <li class="breadcrumb-item active">Edit User</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">
<div class="container-fluid">
<div class="row">
<div class="col-4">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit User</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="/manage/users/{{ $user->id }}">
              @csrf
              @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Enter Full Name" value="{{ $user->name }}">
                  </div>
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="Enter email" value="{{ $user->email }}">
                  </div>
                  <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="Password">
                  </div>
                  <div class="form-group">
                    <label for="password_confirmation">Repeat Password</label>
                    <input type="password" name="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror" id="password_confirmation" placeholder="Confirm Password">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Administrator</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="/manage/users/{{ $user->id }}/save_admin">
              @csrf
              @method('PUT')
                <div class="card-body">

                <div class="custom-control custom-switch">
                <button type="submit" class="btn btn-warning btn-xs float-right">Save</button>
                      <input @if( $user->is_admin ) CHECKED @endif name="is_admin" value="1" type="checkbox" class="custom-control-input" id="customSwitch_user_is_admin">
                      <label class="custom-control-label" for="customSwitch_user_is_admin">Is Admin</label>
                    </div>

                </div>
                <!-- /.card-body -->
                </form>
            </div>
            

</div>
<div class="col-8">

@if( $user->is_admin )

<div class="card card-default">
<form method="POST" action="/manage/users/{{ $user->id }}/permissions">
@csrf
@method('PUT')
              <div class="card-header">
                <h3 class="card-title">Module Permissions</h3>
                <div class="card-tools">
                  <button type="submit" class="btn btn-success btn-xs">Save Changes</button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>Module</th>
@foreach(config('enrollment.actions') as $key=>$actions)
                      <th style="width: 40px">{{ $actions['name'] }}</th>
@endforeach
                    </tr>
                  </thead>
                  <tbody>
@foreach(config('enrollment.modules') as $mkey=>$module)
                    <tr>
                      <td>{{ $module['name'] }}</td>
            @foreach(config('enrollment.actions') as $akey=>$action)
                      <td>
                      
                    <div class="custom-control custom-switch">
                      <input @if( ( isset($user_permissions[$mkey]) ) && ( ($user_permissions[$mkey] % $action['prime']) == 0 ) ) CHECKED @endif name="module[{{$mkey}}][]" value="{{$action['prime']}}" type="checkbox" class="custom-control-input" id="customSwitch_{{ $mkey }}_{{ $akey }}">
                      <label class="custom-control-label" for="customSwitch_{{ $mkey }}_{{ $akey }}"></label>
                    </div>
                  
                      </td>
            @endforeach
                    </tr>
@endforeach

                  </tbody>
                </table>
</form>
              </div>
              <!-- /.card-body -->
            </div>

@endif

</div>


</div>
</div>

</section>

@endsection
