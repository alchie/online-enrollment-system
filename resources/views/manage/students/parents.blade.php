@extends('manage.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h1>Edit Student
            <span class="badge badge-success">{{ $current_item->lastname }}, {{ $current_item->firstname }}</span>
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/manage/students">Students</a></li>
              <li class="breadcrumb-item active">Edit Student</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">
<div class="container-fluid">
<div class="row">
<div class="col-12">


<div class="card card-default card-tabs">
    <div class="card-header">
        @include('manage.students._nav')
    </div>
</div>


</div>
</div>

<div class="row">
          <div class="col-12">
            <div class="card">
@if( ($current_item->parents) && $current_item->parents->isNotEmpty() )
<!--
              <div class="card-header">
                <h3 class="card-title">Students List</h3>

                <div class="card-tools">
                </div>
              </div>
              <#!-- /.card-header -->
              <div class="card-body table-responsive p-0">

                <table class="table table-hover table-striped text-nowrap">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Relationship</th>
                      <th>Occupation</th>
                      <th>Phone</th>
                      <th class="text-right">Action</th>
                    </tr>
                  </thead>
                  <tbody>
@foreach($current_item->parents as $item)
                    <tr>
                      <td>{{$item->name}}</td>
                      <td>{{$item->relationship}}</td>
                      <td>{{$item->occupation}}</td>
                      <td>{{$item->phone}}</td>
                      <td class="text-right">
                      
                      <a href="/manage/students/{{$item->id}}/edit" class="btn btn-warning btn-xs">Edit</a>
                      
                      <a class="btn btn-danger btn-xs confirm_delete" href="javascript:void(0);" data-id="delete-{{$item->id}}">Delete</a>
                      <form id="delete-{{$item->id}}" action="/manage/students/{{ $item->id }}" method="POST" style="display: none;">@csrf @method('DELETE')</form>
                      </td>
                    </tr>
@endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->

@else
<div class="card-body text-center">Nothing found!</div>
@endif
            </div>
            <!-- /.card -->
          </div>
</div>

</div>
</section>
@endsection
