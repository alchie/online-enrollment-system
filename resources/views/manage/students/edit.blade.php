@extends('manage.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Student
            <span class="badge badge-success">{{ $current_item->lastname }}, {{ $current_item->firstname }}</span>
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/manage/students">Students</a></li>
              <li class="breadcrumb-item active">Edit Student</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">
<div class="container-fluid">

<div class="row">

<div class="col-12">


<div class="card card-default card-tabs">
              <div class="card-header">
              @include('manage.students._nav')
                </div>
</div>

</div>


<div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Student</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" action="/manage/students/{{$current_item->id}}">
              @csrf
              @method('PUT')
                <div class="card-body">

<div class="row">
<div class="col-md-4">

                  <div class="form-group">
                    <label for="lastname">Last Name</label>
                    <input value="{{ $current_item->lastname }}" type="text" class="form-control" id="lastname" placeholder="Enter Last Name" name="lastname">
                  </div>
</div>
<div class="col-md-4">

                  <div class="form-group">
                    <label for="firstname">First Name</label>
                    <input value="{{ $current_item->firstname }}" type="text" class="form-control" id="firstname" placeholder="Enter First Name" name="firstname">
                  </div>
</div>
<div class="col-md-4">

                  <div class="form-group">
                    <label for="middlename">Middle Name</label>
                    <input value="{{ $current_item->middlename }}" type="text" class="form-control" id="middlename" placeholder="Enter Middle Name" name="middlename">
                  </div>
</div>
</div>

<div class="row">
<div class="col-md-4">

                  <div class="form-group">
                    <label for="birthday">Birthday</label>
                    <input value="{{ date('m/d/Y', strtotime($current_item->birthday)) }}" type="text" class="form-control" id="birthday" placeholder="Enter Birthday" name="birthday">
                  </div>
</div>
<div class="col-md-4">

                  <div class="form-group">
                    <label for="birthplace">Birth Place</label>
                    <input value="{{ $current_item->birthplace }}" type="text" class="form-control" id="birthplace" placeholder="Enter Birth Place" name="birthplace">
                  </div>
</div>
<div class="col-md-4">

                  <div class="form-group">
                    <label for="religion">Religion</label>
                    <input value="{{ $current_item->religion }}" type="text" class="form-control" id="religion" placeholder="Enter Religion" name="religion">
                  </div>
</div>
</div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

          </div>
</div>

</div>
</section>
@endsection
