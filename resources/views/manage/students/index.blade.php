@extends('manage.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Students
            
                <div class="btn-group">
                <a href="/manage/students/create" class="btn btn-success btn-xs">
            <i class="fa fa-plus"></i>
            Add Student</a>
                    <button type="button" class="btn btn-success btn-xs dropdown-toggle dropdown-icon" data-toggle="dropdown">
                    </button>
                      <span class="sr-only">Toggle Dropdown</span>
                      <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="/manage/students/generate_claim_codes">Generate Claim Codes</a>
                      </div>
                    
                  </div>

            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/manage/dashboard">Home</a></li>
              <li class="breadcrumb-item active">Students</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">
<div class="container-fluid">

<div class="row">
          <div class="col-12">
            <div class="card">
@if( ($items) && $items->isNotEmpty() )
<!--
              <div class="card-header">
                <h3 class="card-title">Students List</h3>

                <div class="card-tools">
                </div>
              </div>
              <#!-- /.card-header -->
              <div class="card-body table-responsive p-0">

                <table class="table table-hover table-striped text-nowrap">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Last Name</th>
                      <th>First Name</th>
                      <th width="5%">Claim Codes</th>
                      <th class="text-right">Action</th>
                    </tr>
                  </thead>
                  <tbody>
@foreach($items as $item)
                    <tr>
                      <td>{{$item->id}}</td>
                      <td>{{$item->lastname}}</td>
                      <td>{{$item->firstname}}</td>
                      <td class="text-center"><a href="/manage/students/{{$item->id}}/claim_codes">{{ $item->claim_codes->count() }}</a></td>
                      <td class="text-right">
                      
                      <a href="/manage/students/{{$item->id}}/edit" class="btn btn-warning btn-xs">Edit</a>
                      
                      <a class="btn btn-danger btn-xs confirm_delete" href="javascript:void(0);" data-id="delete-{{$item->id}}">Delete</a>
                      <form id="delete-{{$item->id}}" action="/manage/students/{{ $item->id }}" method="POST" style="display: none;">@csrf @method('DELETE')</form>
                      </td>
                    </tr>
@endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->

@if( $items->hasPages() )
              <div class="card-footer">{{$items->links()}}</div>
@endif

@else
<div class="card-body text-center">Nothing found!</div>
@endif
            </div>
            <!-- /.card -->
          </div>
</div>

</div>
</section>
@endsection
