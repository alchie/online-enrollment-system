<ul class="nav nav-tabs" role="tablist">   
@foreach([
'edit'=>'Personal Information',
'address_contacts'=>'Address & Contacts',
'academics'=>'Academic Information',
'family'=>'Family Information',
'claim_codes'=>'Claim Codes',
] as $key=>$value)
<li class="nav-item">
    <a class="nav-link {{ ( $current_key == $key ) ? 'active' : '' }}" href="/manage/students/{{  $student_id }}/{{ $key }}">{{ $value }}</a>
</li>
@endforeach

</ul>
