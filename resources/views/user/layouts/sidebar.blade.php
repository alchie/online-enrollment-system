<aside class="main-sidebar <?php echo (session()->get('theme-sidebar')) ? session()->get('theme-sidebar') : 'sidebar-dark-primary'; ?> elevation-4 <?php echo (in_array('main-sidebar-disable', session()->get('theme'))) ? "sidebar-no-expand" : ''; ?>">
    <!-- Brand Logo -->
    <div class="brand-link <?php echo session()->get('theme-brand-logo'); ?> <?php echo (in_array('brand-small-text', session()->get('theme'))) ? "text-sm" : ''; ?>">
      <img src="/storage/img/San_Lorenzo_College_of_Davao_logo_128x128.png"
           alt="{{env('APP_NAME')}}"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">{{env('APP_NAME')}}</span>
    </div>

    <!-- Sidebar -->
    <div class="sidebar">

    @auth
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="/storage/img/avatar.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <span class="d-block <?php echo (session()->get('theme-sidebar') && (strpos(session()->get('theme-sidebar'),'dark'))) ? 'text-white' : ''; ?>">{{ Auth::user()->name }}</span>
        </div>
      </div>
    @endauth
  

<?php 
$sidebar_class = '';
$sidebar_class .= (in_array('sidebar-nav-small-text', session()->get('theme'))) ? "text-sm " : '';
$sidebar_class .= (in_array('sidebar-nav-flat-style', session()->get('theme'))) ? "nav-flat " : '';
$sidebar_class .= (in_array('sidebar-nav-legacy-style', session()->get('theme'))) ? "nav-legacy " : ''; 
$sidebar_class .= (in_array('sidebar-nav-compact', session()->get('theme'))) ? "nav-compact " : '';
$sidebar_class .= (in_array('sidebar-nav-child-indent', session()->get('theme'))) ? "nav-child-indent " : ''; 
?>
    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column {{ $sidebar_class }}" data-widget="treeview" role="menu" data-accordion="false">

        <li class="nav-item">
            <a href="/welcome" class="nav-link @if('welcome'==$current_controller) active @endif">
              <i class="nav-icon fa fa-home"></i>
              <p>Welcome</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="/enrollment" class="nav-link @if('enrollment'==$current_controller) active @endif">
              <i class="nav-icon fa fa-id-card-o"></i>
              <p>Enrollment</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="/students" class="nav-link @if('students'==$current_controller) active @endif">
              <i class="nav-icon fa fa-users"></i>
              <p>My Students</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="/payments" class="nav-link @if('payments'==$current_controller) active @endif">
              <i class="nav-icon fa fa-money"></i>
              <p>My Payments</p>
            </a>
          </li>

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
      
    </div>
    <!-- /.sidebar -->
  </aside>