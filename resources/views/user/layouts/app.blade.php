<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  
  <title>{{ config('app.name', 'Clients Monitoring System') }}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>

<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body class="hold-transition sidebar-mini
 <?php echo (in_array('navbar-fixed', session()->get('theme'))) ? "layout-navbar-fixed" : 'layout-fixed'; ?>
 <?php echo (in_array('footer-fixed', session()->get('theme'))) ? "layout-footer-fixed" : ''; ?>
 <?php echo (in_array('body-small-text', session()->get('theme'))) ? "text-sm" : ''; ?>
 <?php echo (in_array('sidebar-collapsed', session()->get('theme'))) ? "sidebar-collapse" : ''; ?>
 <?php echo session()->get('theme-accent'); ?>
">
<!-- Site wrapper -->
<div class="wrapper" id="app">
  <!-- Navbar -->
  @include('user.layouts.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  @include('user.layouts.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper"> 
    @yield('content')
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer <?php echo (in_array('footer-small-text', session()->get('theme'))) ? "text-sm" : ''; ?>">
    <div class="float-right d-none d-sm-block">
      <b>Developed by <a href="https://www.chesteralan.com/" target="_blank">Chester Alan</a></b>
    </div>
    <strong>{{env('APP_NAME')}} of <a target="_blank" href="{{env('APP_URL')}}">{{env('COMPANY_NAME')}}</a></strong>
  </footer>


</div>
<!-- ./wrapper -->

</body>
</html>
