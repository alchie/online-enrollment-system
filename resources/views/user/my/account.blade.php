@extends('user.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>My Account</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item active">My Account</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">

<div class="container">
<div class="row">
          <div class="col-md-4">
            <div class="card card-primary">
                  <div class="ribbon-wrapper ribbon-lg">
                        <div class="ribbon bg-warning text-lg">
                          P0.00
                        </div>
                      </div>
              <div class="card-header">
                <h3 class="card-title">Free Account</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Benefits</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                   
                    <tr>
                      <td>Sales Commission<br>on Products & Services</td>
                      <td class="text-right"><span class="badge bg-success">5%</span></td>
                    </tr>
                    <tr>
                      <td>Direct Referral Commission<br>on Account Upgrade</td>
                      <td class="text-right"><span class="badge bg-success">P 1.00</span></td>
                    </tr>
                    <tr>
                      <td>Network Downline Income<br>on Account Upgrade</td>
                      <td class="text-right"><span class="badge bg-danger">none</span></td>
                    </tr>
                    <tr>
                      <td>Network Level</td>
                      <td class="text-right"><span class="badge bg-danger">0</span></td>
                    </tr>
                    <tr>
                      <td>Minimum Withdrawal<br>per request</td>
                      <td class="text-right"><span class="badge bg-success">P5,000.00</span></td>
                    </tr>
                    <tr>
                      <td>Maximum Withdrawal<br>per request</td>
                      <td class="text-right"><span class="badge bg-success">P5,000.00</span></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button class="btn btn-primary disabled btn-block">Current Account</button>
              </div>
            </div>
            <!-- /.card -->
          </div>
          <div class="col-md-4">
            <div class="card card-primary">
            <div class="ribbon-wrapper ribbon-lg">
                        <div class="ribbon bg-warning text-lg">
                          P100.00
                        </div>
                      </div>
              <div class="card-header">
                <h3 class="card-title">Premium Account</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Benefits</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                   
                    <tr>
                      <td>Sales Commission<br>on Products & Services</td>
                      <td class="text-right"><span class="badge bg-success">10%</span></td>
                    </tr>
                    <tr>
                      <td>Direct Referral Commission<br>on Account Upgrade</td>
                      <td class="text-right"><span class="badge bg-success">P 5.00</span></td>
                    </tr>
                    <tr>
                      <td>Network Downline Income<br>on Account Upgrade</td>
                      <td class="text-right"><span class="badge bg-success">P 1.00</span></td>
                    </tr>
                    <tr>
                      <td>Network Level</td>
                      <td class="text-right"><span class="badge bg-success">20</span></td>
                    </tr>
                    <tr>
                      <td>Minimum Withdrawal<br>per request</td>
                      <td class="text-right"><span class="badge bg-success">P3,000.00</span></td>
                    </tr>
                    <tr>
                      <td>Maximum Withdrawal<br>per request</td>
                      <td class="text-right"><span class="badge bg-success">P10,000.00</span></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <a href="/my/payments/upgrade_account" class="btn btn-success @if(Auth::user()->account_level > 0) disabled @endif btn-block">Upgrade Account</a>
              </div>
            </div>
            <!-- /.card -->
          </div>

</div>
</div>

</section>
@endsection
