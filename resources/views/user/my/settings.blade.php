@extends('user.layouts.app')

@section('content')

<section class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>My Settings</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item active">My Settings</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">
<div class="container">
<div class="row">
<div class="col-12">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Theme Settings</h3>
                <div class="card-tools">
                  <a href="/my/reset-theme?back={{ request('back') }}" class="btn btn-tool"><i class="fa fa-refresh"></i>
                  </a>
                </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="/my/settings?back={{ request('back') }}">
             @csrf
                <div class="card-body">
<div class="row">
<div class="col-6">
                  <div class="form-group">
                    <label for="name">Navbar Settings</label>
                    <div class="mb-1"><input type="checkbox" name="theme[]" value="no-navbar-border" class="mr-1"@if($no_navbar_border) CHECKED @endif><span>No Navbar border</span></div>
                    <div class="mb-1"><input type="checkbox" name="theme[]" value="navbar-small-text" class="mr-1"@if($navbar_small_text) CHECKED @endif><span>Navbar small text</span></div>
                    <div class="mb-1"><input type="checkbox" name="theme[]" value="navbar-fixed" class="mr-1"@if($navbar_fixed) CHECKED @endif><span>Navbar Fixed</span></div>
                  </div>
                  <div class="form-group">
                    <label for="name">Body Settings</label>
                    <div class="mb-1"><input type="checkbox" name="theme[]" value="body-small-text" class="mr-1"@if($body_small_text) CHECKED @endif><span>Body small text</span></div>
                  </div>
                  <div class="form-group">
                    <label for="name">Sidebar Settings</label>
                    <div class="mb-1"><input type="checkbox" name="theme[]" value="brand-small-text" class="mr-1"@if($brand_small_text) CHECKED @endif><span>Brand small text</span></div>
                    <div class="mb-1"><input type="checkbox" name="theme[]" value="sidebar-nav-small-text" class="mr-1"@if($sidebar_nav_small_text) CHECKED @endif><span>Sidebar nav small text</span></div>
                    <div class="mb-1"><input type="checkbox" name="theme[]" value="sidebar-collapsed" class="mr-1"@if($sidebar_collapsed) CHECKED @endif><span>Sidebar collapsed</span></div>
                    <div class="mb-1"><input type="checkbox" name="theme[]" value="sidebar-nav-flat-style" class="mr-1"@if($sidebar_nav_flat_style) CHECKED @endif><span>Sidebar nav flat style</span></div>
                    <div class="mb-1"><input type="checkbox" name="theme[]" value="sidebar-nav-legacy-style" class="mr-1"@if($sidebar_nav_legacy_style) CHECKED @endif><span>Sidebar nav legacy style</span></div>
                    <div class="mb-1"><input type="checkbox" name="theme[]" value="sidebar-nav-compact" class="mr-1"@if($sidebar_nav_compact) CHECKED @endif><span>Sidebar nav compact</span></div>
                    <div class="mb-1"><input type="checkbox" name="theme[]" value="sidebar-nav-child-indent" class="mr-1"@if($sidebar_nav_child_indent) CHECKED @endif><span>Sidebar nav child indent</span></div>
                    <div class="mb-1"><input type="checkbox" name="theme[]" value="main-sidebar-disable" class="mr-1"@if($main_sidebar_disable) CHECKED @endif><span>Main Sidebar disable hover/focus auto expand</span></div>
                  </div>
                  <div class="form-group">
                    <label for="name">Footer Settings</label>
                    <div class="mb-1"><input type="checkbox" name="theme[]" value="footer-small-text" class="mr-1"@if($footer_small_text) CHECKED @endif><span>Footer small text</span></div>
                    <div class="mb-1"><input type="checkbox" name="theme[]" value="footer-fixed" class="mr-1"@if($footer_fixed) CHECKED @endif><span>Footer Fixed</span></div>
                  </div>
</div>
<div class="col-6">
                  <div class="form-group">
                    <label for="name">Brand Logo Variants</label>
                    <div class="d-flex flex-wrap mb-3">
@foreach (['primary','secondary','info','success','danger','indigo','purple','pink','navy','lightblue','teal','cyan','dark','gray-dark','gray','light','warning','white','orange'] as $color)
                      <a class="bg-{{$color}} elevation-2 color-oval <?php echo (session()->get('theme-brand-logo')=="navbar-".$color) ? "active" : ''; ?>" href="/my/change-color/brand-logo/{{$color}}?back={{ request('back') }}"></a>
@endforeach
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="name">Navbar Dark Variants</label>
                    <div class="d-flex flex-wrap mb-3">
@foreach (['primary','secondary','info','success','danger','indigo','purple','pink','navy','lightblue','teal','cyan','dark','gray-dark','gray'] as $color)
                      <a class="bg-{{$color}} elevation-2 color-oval <?php echo (session()->get('theme-navbar')=="navbar-dark navbar-".$color) ? "active" : ''; ?> <?php echo session()->get('theme-navbar'); ?>" href="/my/change-color/navbar-dark/{{$color}}?back={{ request('back') }}"></a>
@endforeach
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="name">Navbar Light Variants</label>
                    <div class="d-flex flex-wrap mb-3">
@foreach (['light','warning','white','orange'] as $color)
                      <a class="bg-{{$color}} elevation-2 color-oval <?php echo (session()->get('theme-navbar')=="navbar-light navbar-".$color) ? "active" : ''; ?> <?php echo session()->get('theme-navbar'); ?>" href="/my/change-color/navbar-light/{{$color}}?back={{ request('back') }}"></a>
@endforeach
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="name">Accent Color Variants</label>
                    <div class="d-flex flex-wrap mb-3">
@foreach (['primary','warning','info','danger','success','indigo','lightblue','navy','purple','fuchsia','pink','maroon','orange','lime','teal','olive','default'] as $color)
                      <a class="bg-{{$color}} elevation-2 color-oval <?php echo (session()->get('theme-accent')=="accent-".$color) ? "active" : ''; ?> <?php echo session()->get('theme-accent'); ?>" href="/my/change-color/accent/{{$color}}?back={{ request('back') }}"></a>
@endforeach
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="name">Dark Sidebar Variants</label>
                    <div class="d-flex flex-wrap mb-3">
@foreach (['primary','warning','info','danger','success','indigo','lightblue','navy','purple','fuchsia','pink','maroon','orange','lime','teal','olive'] as $color)
                      <a class="bg-{{$color}} elevation-2 color-oval <?php echo (session()->get('theme-sidebar')=="sidebar-dark-".$color) ? "active" : ''; ?> <?php echo session()->get('theme-sidebar'); ?>" href="/my/change-color/dark-sidebar/{{$color}}?back={{ request('back') }}"></a>
@endforeach
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="name">Light Sidebar Variants</label>
                    <div class="d-flex flex-wrap mb-3">
@foreach (['primary','warning','info','danger','success','indigo','lightblue','navy','purple','fuchsia','pink','maroon','orange','lime','teal','olive'] as $color)
                      <a class="bg-{{$color}} elevation-2 color-oval <?php echo (session()->get('theme-sidebar')=="sidebar-light-".$color) ? "active" : ''; ?> <?php echo session()->get('theme-sidebar'); ?>" href="/my/change-color/light-sidebar/{{$color}}?back={{ request('back') }}"></a>
@endforeach
                    </div>
                  </div>
</div></div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
</div>
</div>
</div>
</section>

@endsection
