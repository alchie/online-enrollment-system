@extends('user.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>My Payments
            
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/my/payments">My Payments</a></li>
              <li class="breadcrumb-item active">Account Upgrade</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">
<div class="container">

<div class="row">
<div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Account Upgrade</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" action="/my/payments/upgrade_account">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="payment_method">Payment Method</label>
                    <select class="form-control" name="payment_method">
                    @foreach(config('mlm.payment_methods') as $key => $payment_method)
                          <option value="{{ $key }}">{{ $payment_method['name'] }}</option>
                    @endforeach
                        </select>
                  </div>
                  <div class="form-group">
                    <label for="reference_number">Reference Number</label>
                    <input type="text" class="form-control text-center" id="reference_number" placeholder="Enter Reference Number" name="reference_number">
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="date_deposited">Date Deposited</label>
                        <input value="{{ date('m/d/Y') }}" type="text" class="form-control text-center" id="date_deposited" placeholder="Enter Date (01/01/2020)" name="date_deposited">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="date_deposited">Time Deposited</label>
                        <input value="{{ date('H:i:s') }}" type="text" class="form-control text-center" id="time_deposited" placeholder="Enter Time (01:30:51)" name="date_deposited">
                      </div>
                    </div>
                    </div>
                  <div class="form-group">
                    <label for="amount_deposited">Amount Deposited</label>
                    <input value="100.00" type="text" class="form-control text-right" id="amount_deposited" placeholder="Enter Amount Deposited" name="amount_deposited">
                  </div>
                  <div class="form-group">
                    <label for="notes">Notes</label>
                    <input value="" type="text" class="form-control" id="notes" placeholder="Notes / Remarks" name="notes">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

          </div>

          <div class="col-md-6">
            <div class="card card-primary">
            <div class="ribbon-wrapper ribbon-lg">
                        <div class="ribbon bg-warning text-lg">
                          P100.00
                        </div>
                      </div>
              <div class="card-header">
                <h3 class="card-title">Premium Account</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Benefits</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                   
                    <tr>
                      <td>Sales Commission<br>on Products & Services</td>
                      <td class="text-right"><span class="badge bg-success">10%</span></td>
                    </tr>
                    <tr>
                      <td>Direct Referral Commission<br>on Account Upgrade</td>
                      <td class="text-right"><span class="badge bg-success">P 5.00</span></td>
                    </tr>
                    <tr>
                      <td>Network Downline Income<br>on Account Upgrade</td>
                      <td class="text-right"><span class="badge bg-success">P 1.00</span></td>
                    </tr>
                    <tr>
                      <td>Network Level</td>
                      <td class="text-right"><span class="badge bg-success">20</span></td>
                    </tr>
                    <tr>
                      <td>Minimum Withdrawal<br>per request</td>
                      <td class="text-right"><span class="badge bg-success">P3,000.00</span></td>
                    </tr>
                    <tr>
                      <td>Maximum Withdrawal<br>per request</td>
                      <td class="text-right"><span class="badge bg-success">P10,000.00</span></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              
            </div>
            <!-- /.card -->
          </div>

</div>

</div>
</section>
@endsection
