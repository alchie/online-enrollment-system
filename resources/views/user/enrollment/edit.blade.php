@extends('user.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Enrollment</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/enrollment">Enrollment</a></li>
              <li class="breadcrumb-item active">Edit Enrollment</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">
<div class="container-fluid">

<div class="row">
<div class="col-12">
<div class="card card-default card-tabs">
  <div class="card-header">@include('user.enrollment._nav')</div>
</div>
</div><!-- // col -->
</div><!-- /.row -->

<div class="row">
<div class="col-12">

<div class="card card-primary">
  <div class="card-header">
  <a class="float-right" href="/students/{{ $student->student_id }}/personal">
              <i class="fa fa-edit"></i>
              </a>
    <h3 class="card-title"><strong>PERSONAL INFORMATION</strong></h3>
  </div><!-- /.card-header -->
  <div class="card-body">
    
<div class="row">
  <div class="col-4">
      <div class="form-group">
        <label>Last Name</label>
        <div class="form-control">{{ $student->lastname }}</div>
      </div>
  </div><!-- .col -->
  <div class="col-4">
      <div class="form-group">
        <label>First Name</label>
        <div class="form-control">{{ $student->firstname }}</div>
      </div>
  </div><!-- .col -->
  <div class="col-4">
      <div class="form-group">
        <label>Middle Name</label>
        <div class="form-control">{{ $student->middlename }}</div>
      </div>
  </div><!-- .col -->
</div><!-- .row -->

<div class="row">
  <div class="col-3">
      <div class="form-group">
        <label>Birthday</label>
        <div class="form-control">{{ date("m/d/Y", strtotime($student->birthday)) }}</div>
      </div>
  </div><!-- .col -->
  <div class="col-3">
      <div class="form-group">
        <label>Birthplace</label>
        <div class="form-control">{{ $student->birthplace }}</div>
      </div>
  </div><!-- .col -->
  <div class="col-3">
      <div class="form-group">
        <label>Religion</label>
        <div class="form-control">{{ $student->religion }}</div>
      </div>
  </div><!-- .col -->
  <div class="col-3">
      <div class="form-group">
        <label>Gender</label>
        <div class="form-control">{{ ($student->gender=='m') ? 'Male' : 'Female' }}</div>
      </div>
  </div><!-- .col -->
</div><!-- .row -->

  </div><!-- /.card-body -->
</div><!-- /.card -->
</div><!-- // col -->
</div><!-- /.row -->

<div class="row">
<div class="col-3">
  <div class="card card-primary">
      <div class="card-header">
                <h2 class="card-title"><strong>ENROLLMENT</strong></h2><br>
              </div><!-- /.card-header -->
     <div class="card-body">

     <div class="form-group">
<label for="grade_level">Grade Level</label>
<div class="dropdown">
  <button class="btn btn-secondary btn-block dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  {{ config('enrollment.grade_levels')[$student->grade_level]['name'] }}
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
  @foreach(config('enrollment.grade_levels') as $key=>$level)
  @if( $student->grade_level != $key )
    <a class="dropdown-item" href="/enrollment/{{$student->enrolled_id}}/update/grade_level/{{ $key }}">{{ $level['name'] }}</a>
  @endif
  @endforeach
  </div>
</div>
</div>

<div class="form-group">
<label for="grade_level">Student Status</label>

<div class="dropdown">
  <button class="btn btn-secondary btn-block dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    {{ ucwords($student->status) }}
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
  @foreach(['old' => 'Old', 'new'=>'New'] as $key=>$value)
  @if( $student->status != $key )
    <a class="dropdown-item" href="/enrollment/{{$student->enrolled_id}}/update/status/{{$key}}">{{ $value }}</a>
  @endif
  @endforeach
  </div>
</div>
</div>

     </div>
  </div><!-- // card -->

</div><!-- // col -->
<div class="col-9">

<div class="card card-primary">
              <div class="card-header">
              <a class="float-right" href="/students/{{ $student->student_id }}/address_contacts">
              <i class="fa fa-edit"></i>
              </a>
                <h2 class="card-title"><strong>ADDRESS</strong></h2><br>
                <small></small>
              </div>
              <!-- /.card-header -->

                <div class="card-body">

<div class="row">
<div class="col-6">


                <div class="form-group">
                    <label for="religion">Residential Address:</label>

@if( count($addresses) > 0 )

                    <div class="btn-group">

                    <button type="button" class="btn btn-success btn-xs dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="true">
                    Change </button>
                      <span class="sr-only">Toggle Dropdown</span>
                      <div class="dropdown-menu" role="menu">
                      @foreach($addresses as $address)
                      @if( (!isset($enrolled_residential)) || ($enrolled_residential->id != $address->id) )
                        <a class="dropdown-item" href="/enrollment/{{$student->enrolled_id}}/update/residential/{{$address->id}}">{{$address->address}}, {{$address->zip}}</a>
                      @endif
                      @endforeach
                      </div>
                  </div>

                <div class="form-control">{{ $enrolled_residential->address ?? '' }}
                  <span class="badge badge-success float-right">{{ $enrolled_residential->zip ?? '' }}</span>
                </div>
                

@else

<div class="text-center">
<a class="btn btn-warning btn-xs" href="/students/{{ $student->student_id }}/address_contacts?show=add-form"><i class="fa fa-plus"></i> Add Address</a>
</div>

@endif

</div>

</div><!-- .col -->
<div class="col-6">
                  <div class="form-group">
                    <label for="religion">Provincial Address:</label>
@if( count($addresses) > 0 )

                    <div class="btn-group">
                    <button type="button" class="btn btn-success btn-xs dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="true">
                    Change </button>
                      <span class="sr-only">Toggle Dropdown</span>
                      <div class="dropdown-menu" role="menu">
                      @foreach($addresses as $address)
                      @if( (!isset($enrolled_provincial)) || ($enrolled_provincial->id != $address->id ))
                        <a class="dropdown-item" href="/enrollment/{{$student->enrolled_id}}/update/provincial/{{$address->id}}">{{$address->address}}, {{$address->zip}}</a>
                      @endif
                      @endforeach
                      </div>
                    
                  </div>

                  <div class="form-control">{{ $enrolled_provincial->address ?? '' }}
                  <span class="badge badge-success float-right">{{ $enrolled_provincial->zip ?? '' }}</span>
                </div>

@else
<div class="text-center">
<a class="btn btn-warning btn-xs" href="/students/{{ $student->student_id }}/address_contacts?show=add-form"><i class="fa fa-plus"></i> Add Address</a>
</div>
@endif

                  </div>
</div><!-- .col -->
</div><!-- .row -->

                </div><!-- .card-body -->
            </div><!-- .card -->

</div><!-- // col -->
</div><!-- /.row -->



<div class="row">
<div class="col-12">

<div class="card card-primary">
              <div class="card-header">

@if( $enrolled_academic )

                    <div class="btn-group float-right accent-dark">
                    <button type="button" class="btn btn-success btn-xs dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="true">
                    Change
                    </button>
                      <span class="sr-only">Toggle Dropdown</span>
                      <div class="dropdown-menu dropdown-menu-right" role="menu">
                      @foreach($academics as $academic)
                      @if( $enrolled_academic->id != $academic->id )
                        <a class="dropdown-item text-dark" href="/enrollment/{{ $student->enrolled_id }}/update/academic/{{$academic->id}}">{{$academic->last_attended}}, {{$academic->address}}</a>
                      @endif
                      @endforeach
                      </div>
                    
                  </div>
@else

<a class="float-right" href="/students/{{ $student->student_id }}/academic">
              <i class="fa fa-edit"></i>
              </a>

 @endif


                <h2 class="card-title"><strong>ACADEMIC INFORMATION</strong></h2><br>
                <small></small>
              </div>
              <!-- /.card-header -->

                <div class="card-body">


@if( $enrolled_academic )

<div class="row">
  <div class="col-8"><div class="form-group"><label for="religion">Last School Attended:</label><div class="form-control">{{ $enrolled_academic->last_attended }}</div></div></div>
  <div class="col-4"><div class="form-group"><label for="religion">Grade Level:</label><div class="form-control">{{ config('enrollment.grade_levels')[$enrolled_academic->grade_level]['name'] }}</div></div></div>
  <div class="col-8"><div class="form-group"><label for="religion">Address:</label><div class="form-control">{{ $enrolled_academic->address }}</div></div></div>
  <div class="col-4"><div class="form-group"><label for="religion">School Year:</label><div class="form-control">{{ $enrolled_academic->school_year }}</div></div></div>
</div>



@else

@if( count( $academics) > 0 )
        <div class="form-group">
                    <label for="religion">Select School:</label>

                    <div class="btn-group">
                    <button type="button" class="btn btn-default btn-xs dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="true">
                    </button>
                      <span class="sr-only">Toggle Dropdown</span>
                      <div class="dropdown-menu" role="menu">
                      @foreach($academics as $academic)
                        <a class="dropdown-item" href="/enrollment/{{ $student->enrolled_id }}/update/academic/{{$academic->id}}">{{$academic->last_attended}}, {{$academic->address}}</a>
                      @endforeach
                      </div>
                    
                  </div>
                <div class="form-control">Nothing assigned!</div>
                </div>
@else
<div class="text-center">
<a class="btn btn-warning btn-xs" href="/students/{{ $student->student_id }}/academic?show=add-form"><i class="fa fa-plus"></i> Add Info</a>
</div>
@endif

@endif
                  
                </div>
            </div>

</div><!-- // col -->
</div><!-- /.row -->

<div class="row">
<div class="col-12">

<form method="POST" action="/enrollment/{{ $student->enrolled_id }}/update_family">
@csrf
            <div class="card card-primary">
              <div class="card-header">
              <button type="submit" class="btn btn-success btn-xs float-right">Save</button>
                <h2 class="card-title"><strong>FAMILY INFORMATION</strong></h2><br>
                <small></small>
              </div>
              <!-- /.card-header -->

                <div class="card-body">
<div class="row">
<div class="col-4">
@if( count($fathers) > 0 )

                <div class="form-group">
                    <label for="religion">Father:</label>

                    <div class="btn-group">
                    <button type="button" class="btn btn-success btn-xs dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="true">
                    Change </button>
                      <span class="sr-only">Toggle Dropdown</span>
                      <div class="dropdown-menu" role="menu">
                      @foreach($fathers as $parent)
                      @if( (!isset($enrolled_father)) || ($parent->id != $enrolled_father->id) )
                        <a class="dropdown-item" href="/enrollment/{{$student->enrolled_id}}/update/father/{{$parent->id}}">{{$parent->name}}</a>
                      @endif
                      @endforeach
                      </div>
                    
                  </div>

                  <div class="form-control">{{ $enrolled_father->name ?? 'Not Set' }}</div>
                  
                  </div>

@else
<div class="text-center">
<a class="btn btn-warning btn-xs" href="/students/{{ $student->student_id }}/family?show=add-form"><i class="fa fa-plus"></i> Add Father</a>
</div><br>
@endif
</div>
<div class="col-4">
@if( count($mothers) > 0 )

                  <div class="form-group">
                    <label for="religion">Mother:</label>

                    <div class="btn-group">
                    <button type="button" class="btn btn-success btn-xs dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="true">
                    Change </button>
                      <span class="sr-only">Toggle Dropdown</span>
                      <div class="dropdown-menu" role="menu">
                      @foreach($mothers as $parent)
                      @if( (!isset($enrolled_mother)) || ( $parent->id != $enrolled_mother->id ))
                        <a class="dropdown-item" href="/enrollment/{{$student->enrolled_id}}/update/mother/{{$parent->id}}">{{$parent->name}}</a>
                      @endif
                      @endforeach
                      </div>
                    
                  </div>

                  <div class="form-control">{{ $enrolled_mother->name ?? 'Not Set' }}</div>

                  </div>

@else
<div class="text-center">
<a class="btn btn-warning btn-xs" href="/students/{{ $student->student_id }}/family?show=add-form"><i class="fa fa-plus"></i> Add Mother</a>
</div>
@endif
</div>
<div class="col-4">
@if( count($guardians) > 0 )

                  <div class="form-group">
                    <label for="religion">Guardian:</label>

                    <div class="btn-group">
                    <button type="button" class="btn btn-default btn-xs dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="true">
                    </button>
                      <span class="sr-only">Toggle Dropdown</span>
                      <div class="dropdown-menu" role="menu">
                      @foreach($guardians as $parent)
                      @if( (!isset($enrolled_guardian)) || ( $parent->id != $enrolled_guardian->id ))
                        <a class="dropdown-item" href="/enrollment/{{$student->enrolled_id}}/update/guardian/{{$parent->id}}">{{$parent->name}}</a>
                      @endif
                      @endforeach
                      </div>
                    
                  </div>

                  <div class="form-control">{{ $enrolled_guardian->name ?? 'Not Set' }}</div>

                  </div>

@else
<div class="text-center">
<a class="btn btn-warning btn-xs" href="/students/{{ $student->student_id }}/family?show=add-form"><i class="fa fa-plus"></i> Add Guardian</a>
</div>
@endif
</div>
</div>

<div class="row">
<div class="col-4">
<div class="form-group">
                    <label for="religion">Parents are:</label>
@foreach([
'living-together' => 'Living Together',
'father-abroad' => 'Father Abroad',
'mother-abroad' => 'Mother Abroad',
'separated' => 'Separated',
'others' => 'Other Situation, specify:',
] as $key=>$value )
                    <div class="custom-control custom-radio">
                      <input {{ ( isset($enrolled_parents_status) && ($enrolled_parents_status->meta_value==$key)) ? 'CHECKED' : '' }} class="custom-control-input" type="radio" id="parentsAre-{{ $key }}" name="parents_status" value="{{ $key }}">
                      <label for="parentsAre-{{ $key }}" class="custom-control-label">{{ $value }}</label>
                    </div>
@endforeach
<input type="text" class="form-control" name="parents_status_other" value="{{ $enrolled_parents_status_other->meta_value ?? '' }}" />

</div><!-- form-group -->         
</div><!-- col -->
<div class="col-4">
<div class="form-group">
                    <label for="religion">Student is Living:</label>
@foreach([
'parents' => 'With Parents',
'father' => 'With Father',
'mother' => 'With Mother',
'grand-parents' => 'With Grand Parents',
'relatives' => 'With Relatives',
'guardian' => 'With Other Guardian',
'rented-house' => 'In a Boarding House/ Dormitory / Apartment',
] as $key=>$value )
                    <div class="custom-control custom-radio">
                      <input {{ ( isset($enrolled_living_condition) && ($enrolled_living_condition->meta_value==$key) ) ? 'CHECKED' : '' }} class="custom-control-input" type="radio" id="living_condition-{{ $key }}" name="living_condition" value="{{ $key }}">
                      <label for="living_condition-{{ $key }}" class="custom-control-label">{{ $value }}</label>
                    </div>
@endforeach
</div><!-- form-group -->          
</div><!-- col -->
</div><!-- row -->


</div>
            
            </div>
</form>


</div><!-- // col -->
</div><!-- /.row -->

<div class="row">
<div class="col-6">
<form method="POST" action="/enrollment/{{ $student->enrolled_id }}/update_medical">
@csrf
            <div class="card card-primary">
              <div class="card-header">
              <button type="submit" class="btn btn-success btn-xs float-right">Save</button>
                <h2 class="card-title"><strong>MEDICAL INFORMATION</strong></h2><br>
                <small></small>
              </div>
              <!-- /.card-header -->

                <div class="card-body">

<div class="row">
<div class="col-12">
<div class="form-group">
<label for="religion">Does your child suffer from any of the following?</label>
@foreach([
'allergies' => 'Allergies',
'asthma' => 'Asthma',
'physical-disability' => 'Physical Disability',
'seizure-disorders' => 'Seizure Disorders',
'others' => 'Others, pls. specify:',
] as $key=>$value )
                    <div class="custom-control custom-radio">
                      <input {{ ( isset($enrolled_medical_condition) && ($enrolled_medical_condition->meta_value==$key) ) ? 'CHECKED' : '' }} class="custom-control-input" type="radio" id="medical-{{ $key }}" name="medical_condition" value="{{ $key }}">
                      <label for="medical-{{ $key }}" class="custom-control-label">{{ $value }}</label>
                    </div>
@endforeach
<input type="text" class="form-control" name="medical_condition_other" value="{{ $enrolled_medical_condition_other->meta_value ?? '' }}" />
</div>

</div>

              </div>
                </div>
            
            </div>
</form>
</div><!-- .col -->
<div class="col-6">
<form method="POST" action="/enrollment/{{ $student->enrolled_id }}/update_reason">
@csrf
            <div class="card card-primary">
              <div class="card-header">
              <button type="submit" class="btn btn-success btn-xs float-right">Save</button>
                <h2 class="card-title"><strong>REASONS FOR CHOOSING <br>SAN LORENZO COLLEGE OF DAVAO, INC.</strong></h2><br>
                <small></small>
              </div>
              <!-- /.card-header -->

                <div class="card-body">
<textarea class="form-control" name="reason">{{ $enrolled_reason->meta_value ?? '' }}</textarea>

                </div>
            </div>
</form>

</div><!-- .col -->
</div><!-- .row -->



</div>
</section>
@endsection
