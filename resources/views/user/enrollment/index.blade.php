@extends('user.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ config('enrollment.school_years')[config('enrollment.current_school_year')]['name'] }} Enrollees
@if( $unenrolled )
            <a href="/enrollment/enroll" class="btn btn-success btn-xs">
            <i class="fa fa-plus"></i>
            Enroll a Student</a>
@endif
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/enrollment">Enrollment</a></li>
              <li class="breadcrumb-item active">{{ config('enrollment.school_years')[config('enrollment.current_school_year')]['name'] }}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">
<div class="container">

<div class="row">
@foreach($students as $student) 
<div class="col-md-4">
            <!-- Widget: user widget style 2 -->
            <div class="card card-widget widget-user-2">
            
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header bg-default">
              <a href="/enrollment/{{$student->enrolled_id}}/edit" class="btn btn-warning btn-xs float-right"><i class="fa fa-pencil"></i></a>
                <div class="widget-user-image">
                  <img class="img-circle elevation-2" src="/storage/img/{{ ( $student->gender=='m' ) ? 'avatar' : 'avatar2' }}.png" alt="User Avatar">
                </div>
                <!-- /.widget-user-image -->
                <h3 class="widget-user-username">{{ $student->firstname }}</h3>
                <h5 class="widget-user-desc">{{ $student->lastname }}</h5>
                <span class="badge badge-primary float-right">{{ config('enrollment.grade_levels')[$student->grade_level]['name'] }}</span>
              </div>
              <div class="card-footer p-0">
                <ul class="nav flex-column">
                  <li class="nav-item">
                  <div class="nav-link">
                    <a href="/enrollment/{{ $student->enrolled_id }}/school_fees" class="badge badge-success">School Fees</a>
                    <a href="/enrollment/{{ $student->enrolled_id }}/payments" class="badge badge-success">Payments</a>
                  </div>
                  </li>
                </ul>
              </div>
            </div>
            <!-- /.widget-user -->
          </div>
          <!-- /.col -->
@endforeach
</div>
<!-- /.row -->
        
</div>
</section>
@endsection
