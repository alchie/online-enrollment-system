@extends('user.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Enroll a Student
            <span class="badge badge-success">
            {{ config('enrollment.school_years')[config('enrollment.current_school_year')]['name'] }}
            </span>
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/enrollment">Enrollment</a></li>
              <li class="breadcrumb-item active">{{ config('enrollment.school_years')[config('enrollment.current_school_year')]['name'] }}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">
<div class="container">

<div class="row">
          <div class="col-6">
<form method="post">
@csrf
          <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Initial Details</h3>
              </div>
              <div class="card-body">

<div class="row">
<div class="col-12">
              <div class="form-group">
                <label for="religion">Student:</label>
                <select class="form-control" name="student_id">
                  @foreach($students as $student)
                    <option value="{{ $student->us_id }}">{{ $student->firstname }} {{ $student->lastname }}</option>
                  @endforeach
                </select>
              </div>
</div>

</div>

<div class="row">
<div class="col-7">
              <div class="form-group">
              <label for="religion">Grade Level:</label>
              <select class="form-control" name="grade_level">
                @foreach(config('enrollment.grade_levels') as $key=>$grade_level)
                  <option value="{{ $key }}">{{ $grade_level['name'] }}</option>
                @endforeach
              </select>
              </div>
</div>
<div class="col-5">
              <div class="form-group">
              <label for="student_status">Student Status:</label>
              
                @foreach(['old','new'] as $key)
                    <div class="custom-control custom-radio">
                      <input {{ ($key=='new') ? 'CHECKED' : '' }} class="custom-control-input" type="radio" id="student_status-{{ $key }}" name="student_status" value="{{ $key }}">
                      <label for="student_status-{{ $key }}" class="custom-control-label">{{ ucwords( $key ) }}</label>
                    </div>
                @endforeach
              
              </div>
</div>
</div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </div>


</form>
          </div>
          <!-- /.col -->
</div>
<!-- /.row -->
        
</div>
</section>
@endsection
