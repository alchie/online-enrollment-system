@extends('user.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Online Payments
            <a href="/enrollment/{{ $enrolled_id }}/add_payment" class="btn btn-success btn-xs">Add Payment</a>
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/enrollment">Enrollment</a></li>
              <li class="breadcrumb-item active">Online Payments</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">
<div class="container-fluid">

<div class="row">
          
          <div class="col-12">


<div class="card card-default card-tabs">
              <div class="card-header">
              @include('user.enrollment._nav')
                </div></div>
</div>


</div>
<!-- /.row -->


<div class="row">
          <div class="col-12">
            <div class="card">
@if( ($items) && $items->isNotEmpty() )
<!--
              <div class="card-header">
                <h3 class="card-title">Students List</h3>

                <div class="card-tools">
                </div>
              </div>
              <#!-- /.card-header -->
              <div class="card-body table-responsive p-0">

                <table class="table table-hover table-striped text-nowrap">
                  <thead>
                    <tr>
                      
                      <th>Payment Type</th>
                      <th>Payment Method</th>
                      <th>Transaction ID</th>
                      <th>Date / Time</th>
                      <th>Amount</th>
                      <th class="text-right">Status</th>
                      <th width="1%"></th>
                    </tr>
                  </thead>
                  <tbody>
@foreach($items as $item)

                    <tr>
                      <td>{{ config('enrollment.payment_types')[$item->payment_type]['name'] }}</td>
                      <td>{{ config('enrollment.payment_methods')[$item->payment_method]['name'] }}</td>
                      <td>{{$item->reference_id}}</td>
                      <td>{{ date("m/d/Y H:i:s", strtotime($item->date_deposited)) }}</td>
                      <td>{{ number_format($item->amount,2) }}</td>
                      <td class="text-right">
                      <span class="badge badge-{{ ($item->status=='pending') ? 'warning' : 'success' }}">{{ ucwords($item->status) }}</span>
                      </td>
                      <td class="text-right">
@if( $item->status == 'pending' ) 
                      <a class="confirm_delete" href="javascript:void(0);" data-id="delete-{{$item->id}}"><i class="fa fa-trash text-danger"></i></a>
                      <form id="delete-{{$item->id}}" action="/enrollment/{{ $enrolled_id }}/payments/{{$item->id}}" method="POST" style="display: none;">@csrf @method('DELETE')</form>
@endif                      
                      </td>
                    </tr>
@endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->

@if( $items->hasPages() )
              <div class="card-footer">{{$items->links()}}</div>
@endif

@else
<div class="card-body text-center">Nothing found!</div>
@endif
            </div>
            <!-- /.card -->
          </div>
</div>




</div>
</section>
@endsection
