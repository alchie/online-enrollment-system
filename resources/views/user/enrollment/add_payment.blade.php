@extends('user.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Online Payments
            <a href="/enrollment/{{ $enrolled_id }}/add_payment" class="btn btn-success btn-xs">Add Payment</a>
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/enrollment">Enrollment</a></li>
              <li class="breadcrumb-item active">Online Payments</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">
<div class="container-fluid">

<div class="row">
          
          <div class="col-12">


<div class="card card-default card-tabs">
              <div class="card-header">
              @include('user.enrollment._nav')
                </div>
</div>

</div>

</div>
<!-- /.row -->

<div class="row">
          
          <div class="col-12">

<!-- general form elements -->
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Payment</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" action="/enrollment/{{ $enrolled_id }}/payments">
              @csrf
                <div class="card-body">

<div class="row">
          <div class="col-6">

                  <div class="form-group">
                    <label for="name">Payment Type</label>
                    <div class="form-control">Downpayment</div>
                  </div>

                  <div class="form-group">
                    <label for="name">Payment Method</label>
                    <select class="form-control" name="payment_method">
                        <option value="online_banking_bdo">Online Banking - BDO</option>
                        <option value="online_banking_pnb">Online Banking - PNB</option>
                        <option value="online_banking_lbp">Online Banking - Landbank</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="name">Notes / Remarks</label>
                    <input placeholder="Enter Notes" type="text" class="form-control" name="notes" />
                  </div>

</div>
<div class="col-6">
                <div class="form-group">
                    <label for="name">Reference / Transaction Number</label>
                    <input placeholder="Enter Transaction Number" type="text" class="form-control" name="reference_number" />
                  </div>
                
                  <div class="form-group">
                    <label for="name">Transaction Date</label>
                    <input placeholder="Enter Transaction Date" type="text" class="form-control" name="transaction_date" />
                  </div>

                  <div class="form-group">
                    <label for="name">Transaction Amount</label>
                    <input placeholder="Enter Transaction Amount" type="text" class="form-control" name="amount" />
                  </div>

</div>
</div> 
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

            </div>
            </div>

        
</div>
</section>
@endsection
