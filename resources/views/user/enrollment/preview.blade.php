@extends('user.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Preview Enrollment Form
            <a target="_print" href="/enrollment/{{ $enrolled_id }}/print?action=print" class="btn btn-warning btn-xs"><i class="fa fa-print"></i> Print Form</a>
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/enrollment">Enrollment</a></li>
              <li class="breadcrumb-item active">Preview Enrollment Form</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">
<div class="container-fluid">

<div class="row">
          
          <div class="col-12">


          <div class="card card-default card-tabs">
              <div class="card-header">
                @include('user.enrollment._nav')
              </div>
                <div class="card-body">
<iframe src="/enrollment/{{ $enrolled_id }}/print" style="
width: 100%;
height: 1500px;
border: none;
" />
                </div>
            </div>
</div>


</div>
<!-- /.row -->
        
</div>
</section>
@endsection
