<ul class="nav nav-tabs" role="tablist">   
@foreach([
'edit'=>'Edit',
'preview'=>'Preview',
'school_fees'=>'School Fees',
'payments'=>'Payments',
] as $key=>$value)
<li class="nav-item">
    <a class="nav-link {{ ( $current_key == $key ) ? 'active' : '' }}" href="/enrollment/{{  $enrolled_id }}/{{ $key }}">{{ $value }}</a>
</li>
@endforeach

</ul>
