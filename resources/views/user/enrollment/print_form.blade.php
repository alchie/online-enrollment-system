<html>
<head>
<title>Preview Enrollment Form</title>
<link href="/css/app.css" rel="stylesheet">
<style>
*,p,h1,h2,h3,h4,h5,h6,div,table,tr,th,td {
    margin:0;
    padding:0;
    font-family: sans-serif;
    font-size:10px;
    font-weight:normal;
    text-align: left;
    vertical-align: top;
}
.margin10 {
    margin-top: 10px;
}
.fontbold {
    font-weight: bold;
}
table {
    width:100%;
    border-left: 2px solid #000;
    border-right: 2px solid #000;
    border-top: 2px solid #000;
    border-bottom: 2px solid #000;
}
td {
    padding: 10px 10px;
    width: 8.33333333333%;
    font-size: 12px;
    border-left: 1px solid #CCC;
}
.top-border {
    border-top: 1px solid #CCC;
}
td:first-child {
    border-left: none;
}
.label {
    text-transform:uppercase;
    font-size: 8px;
    margin-bottom: 5px;
}
div.header {
    text-align: center;
    position:relative;
}
div.header h1 {
    text-align: center;
}
div.header p {
    text-align: center;
}
div.header h3 {
    text-align: center;
}
div.header h1 {
    font-size:20px;
    font-weight: bold;
}
div.header .title {
    font-size: 18px;
}
.department {
    text-align: center;
    padding-top: 15px;
}
td.heading {
    font-weight: bold;
    font-size: 12px;
    text-transform: uppercase;
    background-color:#CCC;
    padding:10px;
    letter-spacing: 5px;
    border-top: 1px solid #000;
    border-bottom: 1px solid #000;
}
td.heading.normal {
    background-color:#FFF;
    text-transform: initial;
    letter-spacing: initial;
    font-weight: normal;
}
div.body {
    padding: 5px;
}
tr.hidden td {
    padding:0;
    border:none;
}
.logo {
    position:absolute;
    left: 5px;
    top: 5px;
    width: 90px;
}
p.agreement {
    font-size: 10px;
    margin-top: 10px;
}
.dark-border-top {
    border-top: 1px solid #000;
}
.dark-border-bottom {
    border-bottom: 1px solid #000;
}
.dark-border-left {
    border-left: 1px solid #000;
}
.dark-border-right {
    border-right: 1px solid #000;
}
.text-center {
    text-align: center;
}
.text-strong {
    font-weight: bold;
}
.text-uppercase {
    text-transform: uppercase;
}
.text-underline {
    text-decoration: underline;
}
.text-font10 {
    font-size: 10px;
}
.text-font12 {
    font-size: 12px;
}
.text-font14 {
    font-size: 14px;
}
.picture_box {
    border: 2px solid #000;
    width: 100px;
    height: 100px;
    padding-top: 30px;
    position:absolute;
    right:10px;
    top:25px;
    text-align:center;
    vertical-align:middle;
}
</style>
</head>
<body>
<div class="header">
<div class="picture_box">1x1<br>Picture</div>
<img src="/storage/img/San_Lorenzo_College_of_Davao_logo_128x128.png" class="logo">
<h1>SAN LORENZO COLLEGE OF DAVAO, INC.</h1>
<p>Lorenzville Homes, Ulas, Brgy. Talomo, Davao City</p>
<p>Tel. No. (082) 233–0848 &middot; Email Address: sanlorenzocollege@yahoo.com &middot; Website: www.slcd.edu.ph</p>

<h3 class="title margin10 fontbold">APPLICATION FOR ADMISSION</h3>
<h3 class="title fontbold">Kindergarten &middot; Grade School &middot; Junior High School &middot; Senior High School</h3>
<h3 class="title fontbold">{{ config('enrollment.school_years')[config('enrollment.current_school_year')]['name'] }}</h3>
</div>

<div class="body">

<table cellspacing="0" cellpadding="0" class="margin10">
    <tr class="hidden">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="3" class="heading dark-border-right">INSTRUCTIONS</td>
        <td colspan="4" class="heading text-center dark-border-right">REQUIREMENTS FOR APPLICATION</td>
        <td colspan="4" class="heading text-center dark-border-right">ESC REQUIREMENTS</td>
        <td colspan="1" rowspan="2" class="text-center"><br><br>
        <div class="label text-center">Student ID:</div>
        {{ $student->idn }} <br><br><br>
        <div class="label text-center">Student Status:</div>
        <i class="{{ ( $student->status == 'old' ) ? 'fa fa-check-square-o' : 'fa fa-square-o' }}"></i> Old
        <br><i class="{{ ( $student->status == 'new' ) ? 'fa fa-check-square-o' : 'fa fa-square-o' }}"></i> New
        
        </td>
    </tr>
    <tr>
        <td colspan="3" class="dark-border-right">1. Fill out this form in PRINT.<br>
        2. Submit fully accomplished form with the requirements to the Registrar.<br>
        3. Pay at the Cashier's Office.</td>
        <td colspan="4" class="text-center dark-border-right">
        1. 3 pcs. 1x1 ID Picture <br>
        2. Filled Out Application for Admission Form <br>
        3. Report Card (Form 138)  <br>
        4. Certificate of Good Moral <br>
        5. NSO Birth Certificate <br>
        6. Recommendation Form
        </td>
        <td colspan="4" class="text-center dark-border-right">
        1. Barangay Certificate Under Low Income  <br>
2. ITR for Parents <br>
3. Filled out ESC Form and Contract
        </td>
        
    </tr>
   
</table>

<table cellspacing="0" cellpadding="0" class="margin10">
    <tr>
        <td colspan="12" class="heading">DEPARTMENT</td>
    </tr>
    <tr class="hidden">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="2" class="department"><i class="{{ ( in_array( $student->grade_level,  ['K1','K2'] ) ) ? 'fa fa-check-square-o' : 'fa fa-square-o' }}"></i> Kindergarten</td>
        <td colspan="2" class="department"><i class="{{ ( in_array( $student->grade_level,  ['G1','G2','G3','G4','G5','G6'] ) ) ? 'fa fa-check-square-o' : 'fa fa-square-o' }}"></i> Elementary</td>
        <td colspan="2" class="department"><i class="{{ ( in_array( $student->grade_level,  ['G7','G8','G9','G10'] ) ) ? 'fa fa-check-square-o' : 'fa fa-square-o' }}"></i> Junior High School</td>
        <td colspan="2" class="department"><i class="{{ ( in_array( $student->grade_level,  ['G11','G12'] ) ) ? 'fa fa-check-square-o' : 'fa fa-square-o' }}"></i> Senior High School</td>
        <td colspan="2"><div class="label">Enrolling in Grade Level:</div>
        {{ config('enrollment.grade_levels')[$student->grade_level]['name'] }}
        </td>
        <td colspan="2"><div class="label">Examination Date & Time:</div>
        </td>
    </tr>
    <tr>
        <td colspan="12" class="heading">PERSONAL INFORMATION</td>
    </tr>
    <tr>
        <td colspan="3"><div class="label">Last Name:</div>  {{ $student->lastname }}
        </td>
        <td colspan="3"><div class="label">First Name:</div> {{ $student->firstname }}
        </td>
        <td colspan="3"><div class="label">Middle Name:</div> {{ $student->middlename }}
        </td>
        <td colspan="3"><div class="label">Gender:</div> 
        <i class="{{ ( $student->gender == 'm' ) ? 'fa fa-check-square-o' : 'fa fa-square-o' }}"></i> Male
        <i style="margin-left:20px;" class="{{ ( $student->gender == 'f' ) ? 'fa fa-check-square-o' : 'fa fa-square-o' }}"></i> Female
        </td>
    </tr>
    <tr class="top-border">
        <td colspan="3"><div class="label">Birthdate:</div>
        {{ date("F d, Y", strtotime($student->birthday)) }}
        </td>
        <td colspan="3"><div class="label">Age:</div>
        {{ $age }} years old
        </td>
        <td colspan="3"><div class="label">Birthplace:</div>
        {{ $student->birthplace }}
        </td>
        <td colspan="3"><div class="label">Religion:</div>
        {{ $student->religion }}
        </td>
    </tr>
    <tr>
        <td colspan="12" class="heading">ADDRESS</td>
    </tr>
@if( $enrolled_residential )
    <tr>
        <td colspan="8"><div class="label">Residential Address:</div>
        {{ $enrolled_residential->address }}
        </td>
        <td colspan="4"><div class="label">Zip Code:</div>
        {{ $enrolled_residential->zip }}
        </td>
    </tr>
@endif
@if( $enrolled_provincial )
    <tr class="top-border">
        <td colspan="8"><div class="label">Provincial Address:</div>
        {{ $enrolled_provincial->address }}
        </td>
        <td colspan="4"><div class="label">Zip Code:</div>
        {{ $enrolled_provincial->zip }}
        </td>
    </tr>
@endif
    <tr>
        <td colspan="12" class="heading">ACADEMIC INFORMATION</td>
    </tr>
@if( $enrolled_academic )
    <tr>
        <td colspan="8"><div class="label">School Last Attended:</div>
        {{ $enrolled_academic->last_attended }}
        </td>
        <td colspan="4"><div class="label">Grade Level:</div>
        {{ config('enrollment.grade_levels')[$enrolled_academic->grade_level]['name'] }}
        </td>
    </tr>
    <tr class="top-border">
        <td colspan="8"><div class="label">School Address:</div>
        {{ $enrolled_academic->address }}
        </td>
        <td colspan="4"><div class="label">School Year:</div>
        {{ $enrolled_academic->school_year }}
        </td>
    </tr>
@endif
    <tr>
        <td colspan="12" class="heading">FAMILY INFORMATION</td>
    </tr>
@if( $enrolled_father )
    <tr>
        <td colspan="6"><div class="label">Father's Name:</div>
        {{ $enrolled_father->name }}
        </td>
        <td colspan="3"><div class="label">Father's Occupation:</div>
        {{ $enrolled_father->occupation }}
        </td>
        <td colspan="3"><div class="label">Father's Contact Number:</div>
        {{ $enrolled_father->phone }}
        </td>
    </tr>
@endif
@if( $enrolled_mother )
    <tr class="top-border">
        <td colspan="6"><div class="label">Mother's Name:</div>
        {{ $enrolled_mother->name }}
        </td>
        <td colspan="3"><div class="label">Mother's Occupation:</div>
        {{ $enrolled_mother->occupation }}
        </td>
        <td colspan="3"><div class="label">Mother's Contact Number:</div>
        {{ $enrolled_mother->phone }}
        </td>
    </tr>
@endif
@if( $enrolled_parents_status )
    <tr class="top-border">
        <td colspan="12"><div class="label">Parents are:</div>
@foreach([
'living-together' => 'Living Together',
'father-abroad' => 'Father Abroad',
'mother-abroad' => 'Mother Abroad',
'separated' => 'Separated',
'others' => 'Other Situation, specify:',
] as $key=>$value )
        <i style="margin-left: 40px;" class="{{ ($enrolled_parents_status->meta_value==$key) ? 'fa fa-check-square-o' : 'fa fa-square-o' }}"></i> {{ $value }}
@endforeach
@if($enrolled_parents_status->meta_value=='others')
<u style="font-size: 12px;">{{ $enrolled_parents_status_other->meta_value }}</u>
@endif
        </td>
    </tr>
@endif
@if( $enrolled_living_condition )
    <tr class="top-border">
        <td colspan="12"><div class="label">Living Arrangement:</div>
        @foreach([
'parents' => 'With Parents',
'father' => 'With Father',
'mother' => 'With Mother',
'grand-parents' => 'With Grand Parents',
'relatives' => 'With Relatives',
'guardian' => 'With Other Guardian',
'rented-house' => 'In a Boarding House/ Dormitory / Apartment',
] as $key=>$value )
        <i style="margin-left: 40px;" class="{{ ($enrolled_living_condition->meta_value==$key) ? 'fa fa-check-square-o' : 'fa fa-square-o' }}"></i> {{ $value }}
@endforeach
        </td>
    </tr>
@endif
@if( $enrolled_guardian )
    <tr class="top-border">
        <td colspan="6"><div class="label">Guardian's Name:</div>
        {{ $enrolled_guardian->name }}
        </td>
        <td colspan="3"><div class="label">Guardian's Occupation:</div>
        {{ $enrolled_guardian->occupation }}
        </td>
        <td colspan="3"><div class="label">Guardian's Contact Number:</div>
        {{ $enrolled_guardian->phone }}
        </td>
    </tr>
@endif
    <tr>
        <td colspan="12" class="heading">MEDICAL INFORMATION</td>
    </tr>
@if( $enrolled_medical_condition )
    <tr>
        <td colspan="12"><div class="label">Does your child suffer from any of the following?</div>
@foreach([
'allergies' => 'Allergies',
'asthma' => 'Asthma',
'physical-disability' => 'Physical Disability',
'seizure-disorders' => 'Seizure Disorders',
'others' => 'Others, pls. specify:',
] as $key=>$value )
        <i style="margin-left: 40px;" class="{{ ($enrolled_medical_condition->meta_value==$key) ? 'fa fa-check-square-o' : 'fa fa-square-o' }}"></i> {{ $value }}        
@endforeach
@if($enrolled_medical_condition->meta_value=='others')
<u style="font-size: 12px;">{{ $enrolled_medical_condition_other->meta_value }}</u>
@endif
        </td>
    </tr>
@endif
@if( $enrolled_reason )
    <tr>
        <td colspan="4" class="heading">REASON FOR CHOOSING SLCDI</td>
        <td colspan="8" class="heading normal">{{ $enrolled_reason->meta_value }}</td>
    </tr>
@endif

</table>

<p class="agreement">I hereby certify that I have read and understood all the instructions and procedures concerning this enrollment form. Furthermore, I also certify that all the
information supplied herein is complete and accurate based on the documents presented. I fully understand that all the documents submitted in support of this
enrollment form will now be considered as property of San Lorenzo College of Davao, Inc. and will therefore not be returned to the student.</p>
<br>
<div class="text-center">______________________________________________________</div>
<div class="label text-center text-font10">Parent's Signature Over Printed Name</div>


<table cellspacing="0" cellpadding="0" class="margin10">
    <tr class="hidden">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="4" class="heading text-center">Registrar</td>
        <td colspan="4" class="heading text-center dark-border-left dark-border-right">Accounting</td>
        <td colspan="4" class="heading text-center">Principal</td>
    </tr>
    <tr>
        <td colspan="4">
<div class="text-font12">
        (&nbsp;&nbsp;&nbsp;) Complete School Requirements<br>
        (&nbsp;&nbsp;&nbsp;) With Lacking School Requirements<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="label">Specify:</span>
        _______________________________<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_______________________________<br>
</div>
<br><br>
        <div class="text-strong text-uppercase text-underline text-center text-font12">MS. RICHELYN A. OBILLO</div>
        <div class="label text-center">Registrar</div>
        </td>
        <td colspan="4" class="text-center dark-border-left dark-border-right">
<div class="text-font12"><br>
OR Number: &nbsp;&nbsp;&nbsp;&nbsp;________________________<br>
Payment Date: ________________________<br>
Amount Paid: &nbsp;&nbsp;________________________
</div>
<br><br>
        <div class="text-strong text-uppercase text-underline text-center text-font12">MRS. MILDRED B. ABULOC</div>
        <div class="label text-center">Accounting Staff</div>

        </td>
        <td colspan="4" class="text-center">
        <div class="label">Approved by:</div>
<br><br><br><br>
        <div class="text-strong text-uppercase text-underline text-center text-font12">SR. MA. FLORIFES C. BEJOD, TDM</div>
        <div class="label text-center">School Principal</div>
        </td>
    </tr>
</table>

</div>
@if( request()->query('action') == 'print' )
<script>
<!--
window.print();
-->
</script>
@endif
</body>
<html>