@extends('user.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>School Fees
            <span class="badge badge-success">{{ config('enrollment.school_years')[config('enrollment.current_school_year')]['name'] }}</span>
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/enrollment">Enrollment</a></li>
              <li class="breadcrumb-item active">School Fees</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">
<div class="container-fluid">

<div class="row">
          
<div class="col-12">


<div class="card card-default card-tabs">
              <div class="card-header">
              @include('user.enrollment._nav')
                </div>
</div>

</div>

<div class="col-4">

<div class="card card-outline card-primary">
  <div class="card-header">
    <h3 class="card-title">{{ config('enrollment.grade_levels')[$enrolled->grade_level]['name'] }} Fees</h3>
  </div>
  <!-- /.card-header -->
  <div class="card-body p-0">
                <ul class="nav flex-column">
                <li class="nav-item">
                  <div class="nav-link">
                        Downpayment <span class="float-right badge bg-primary">{{ (isset($school_fees['downpayment']['amount'])) ? number_format($school_fees['downpayment']['amount'],2) : '0.00' }}</span>
                  </div>
                </li>
@foreach([
6,7,8,9,10,11,12,1,2,3
] as $month)
                  <li class="nav-item">
                    <div class="nav-link">
                    {{ date("F", strtotime($month."/1/2000")) }} {{ $school_fees[strtolower(date("F", strtotime($month."/1/2000")))]['year'] ?? '' }}<span class="float-right badge bg-primary">{{ (isset( $school_fees[strtolower(date("F", strtotime($month."/1/2000")))]['amount'] )) ? number_format($school_fees[strtolower(date("F", strtotime($month."/1/2000")))]['amount'],2) : '0.00' }}</span>
                    </div>
                  </li>
@endforeach
<li class="nav-item">
  <div class="nav-link">
        <strong>Total Fees</strong> <span class="float-right badge bg-primary">{{ number_format( ($total_fees ?? '0.00'), 2 ) }}</span>
  </div>
</li>
                </ul>
              </div>
</div>

</div>



</div>
<!-- /.row -->
        
</div>
</section>
@endsection
