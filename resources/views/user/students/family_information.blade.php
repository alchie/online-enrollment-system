@extends('user.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Student
            <span class="badge badge-success">{{ $student->lastname }}, {{ $student->firstname }}</span>
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/students">My Students</a></li>
              <li class="breadcrumb-item active">Edit Student</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">
<div class="container-fluid">

<div class="row">
          <div class="col-3">
          @include('user.students._nav')
          </div>
          <div class="col-9">

          <div class="card card-primary">
              <div class="card-header">
              <a href="/students/{{ $student->id }}/family?show=add-form" class="float-right"><i class="fa fa-plus"></i></a>
                <h2 class="card-title"><strong>FAMILY INFORMATION</strong></h2><br>
                <small></small>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              
                <div class="card-body">

@foreach($student->parents as $parent)
                  <div class="callout callout-info">
                      {{ $parent->name }} 
                      <span class="badge badge-info">{{ ucwords($parent->relationship) }}</span>
                      <span class="badge badge-info">{{ $parent->occupation }}</span>
                      <span class="badge badge-info">{{ $parent->phone }}</span>
                      <a href="/students/{{ $parent->id }}/edit_parent">
                      <i class="fa fa-pencil float-right"></i>
                    </a>
                  </div>
@endforeach

                </div>
</div>

@if( Request::query('show')=='add-form' )

<form role="form" method="POST">
@csrf
<div class="card card-warning">
              <div class="card-header">
                <h2 class="card-title"><strong><i class="fa fa-plus"></i> ADD NEW</strong></h2><br>
                <small></small>
              </div>
<div class="card-body">

<div class="row">
  <div class="col-12">
                    <div class="form-group">
                      <label for="full_name">Full Name</label>
                      <input value="" type="text" class="form-control" name="full_name" id="full_name" placeholder="Enter Full Name">
                    </div>
  </div>
</div>
<div class="row">
  <div class="col-4">
                    <div class="form-group">
                      <label for="first_name">Relationship</label>
                      <select class="form-control" name="relationship">
                      @foreach([
                        'father'=>'Father',
                        'mother'=>'Mother',
                        'guardian'=>'Guardian'
                      ] as $key=>$value)
                        <option value="{{$key}}">{{$value}}</option>
                      @endforeach
                      </select>
                    </div>
  </div>
  <div class="col-4">
                    <div class="form-group">
                      <label for="occupation">Occupation</label>
                      <input value="" type="text" class="form-control" name="occupation" id="occupation" placeholder="Enter Occupation">
                    </div>
  </div>
  <div class="col-4">
                    <div class="form-group">
                      <label for="phone">Phone Number</label>
                      <input value="" type="text" class="form-control" name="phone" id="phone" placeholder="Enter Phone Number">
                    </div>
  </div>
</div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              
            </div>
</form>

@endif
 


          </div>
          <!-- /.col -->
</div>
<!-- /.row -->
        
</div>
</section>
@endsection
