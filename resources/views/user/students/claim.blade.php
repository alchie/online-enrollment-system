@extends('user.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Claim Old Student</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/students">My Students</a></li>
              <li class="breadcrumb-item active">Claim Old Student</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">
<div class="container">
<div class="row">
<div class="col-6">

<form method="get">
@csrf
<div class="card card-primary">
    <div class="card-header">
    <h3 class="card-title">Claim Code</h3>
    </div>
    <div class="card-body">

    <div class="input-group mb-3">
                  <input value="{{ request()->query('code') }}" type="text" class="form-control" name="code" placeholder="Enter Claim Code">
                  <div class="input-group-append">
                    <button class="input-group-text" type="submit">Submit
                    </button>
                  </div>
                </div>
    <div class="alert alert-danger"><strong>Reminder:</strong> The claim code can only be used once!</div>
    </div>
    <!-- /.card-body -->
</div>
</form>

</div>

@if($student)
<div class="col-12">

<form method="post">
@csrf
<input type="hidden" name="code" value="{{ request()->query('code') }}">
<div class="card card-success">
              <div class="card-header">
                <h2 class="card-title">Student Found</h2>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">

<div class="row">
<div class="col-3">
                  <div class="form-group">
                    <label for="last_name">ID Number</label>
                    <div class="form-control">{{ $student->idn }}</div>
                  </div>
</div>
<div class="col-3">
                  <div class="form-group">
                    <label for="last_name">Last Name</label>
                    <div class="form-control">{{ $student->lastname }}</div>
                  </div>
</div>
<div class="col-3">
                  <div class="form-group">
                  <label for="last_name">First Name</label>
                    <div class="form-control">{{ $student->firstname }}</div>
                  </div>
</div>
<div class="col-3">
                  <div class="form-group">
                  <label for="last_name">Middle Name</label>
                    <div class="form-control">{{ $student->middlename }}</div>
                  </div>
</div>
</div>


                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Claim Student</button>
<br><br>
                  <div class="alert alert-danger"><strong>Reminder:</strong> The claim code will be deleted once the button is clicked.</div>

                </div>

               

              </form>
            </div>

</form>

</div>
@endif

</div>       
</div>
</section>
@endsection
