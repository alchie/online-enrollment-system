@extends('user.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Student</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/students">My Students</a></li>
              <li class="breadcrumb-item active">Edit Student</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">
<div class="container-fluid">

<div class="row">
          <div class="col-3">
          @include('user.students._nav')
          </div>
          <div class="col-9">

<form method="POST">
@csrf
<div class="card card-success">
              <div class="card-header">
                <h2 class="card-title"><strong><i class="fa fa-pencil"></i> EDIT ADDRESS</strong></h2><br>
                <small></small>
              </div>
<div class="card-body">

<div class="row">
          <div class="col-10">
                  <div class="form-group">
                    <label for="address">Address</label>
                    <input value="{{ $address->address }}" type="text" class="form-control" name="address" id="address" placeholder="Enter Address">
                  </div>
          </div>
          <div class="col-2">
                  <div class="form-group">
                    <label for="zip">Zip Code</label>
                    <input value="{{ $address->zip }}" type="text" class="form-control" name="zip" id="zip" placeholder="Enter Zip">
                  </div>
          </div>
</div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
             
            </div>
</form>

          </div>
          <!-- /.col -->
</div>
<!-- /.row -->
        
</div>
</section>
@endsection
