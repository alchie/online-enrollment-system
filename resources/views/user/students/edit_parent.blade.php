@extends('user.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Student</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/students">My Students</a></li>
              <li class="breadcrumb-item active">Edit Student</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">
<div class="container-fluid">

<div class="row">
          <div class="col-3">
              @include('user.students._nav')
          </div>
          <div class="col-9">

<form role="form" method="POST">
@csrf

<div class="card card-success">
              <div class="card-header">
                <h2 class="card-title"><strong><i class="fa fa-pencil"></i> EDIT PARENT</strong></h2><br>
                <small></small>
              </div>
<div class="card-body">

<div class="row">
  <div class="col-12">
                    <div class="form-group">
                      <label for="full_name">Full Name</label>
                      <input value="{{ $parent->name }}" type="text" class="form-control" name="full_name" id="full_name" placeholder="Enter Full Name">
                    </div>
  </div>
</div>
<div class="row">
  <div class="col-4">
                    <div class="form-group">
                      <label for="first_name">Relationship</label>
                      <select class="form-control" name="relationship">
                      @foreach([
                        'father'=>'Father',
                        'mother'=>'Mother',
                        'guardian'=>'Guardian'
                      ] as $key=>$value)
                        <option {{ ($parent->relationship==$key) ? 'SELECTED' : '' }} value="{{$key}}">{{$value}}</option>
                      @endforeach
                      </select>
                    </div>
  </div>
  <div class="col-4">
                    <div class="form-group">
                      <label for="occupation">Occupation</label>
                      <input value="{{ $parent->occupation }}" type="text" class="form-control" name="occupation" id="occupation" placeholder="Enter Occupation">
                    </div>
  </div>
  <div class="col-4">
                    <div class="form-group">
                      <label for="phone">Phone Number</label>
                      <input value="{{ $parent->phone }}" type="text" class="form-control" name="phone" id="phone" placeholder="Enter Phone Number">
                    </div>
  </div>
</div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              
            </div>
</form>

          </div>
          <!-- /.col -->
</div>
<!-- /.row -->
        
</div>
</section>
@endsection
