@extends('user.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Student
            <span class="badge badge-success">{{ $student->lastname }}, {{ $student->firstname }}</span>
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/students">My Students</a></li>
              <li class="breadcrumb-item active">Edit Student</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">
<div class="container-fluid">

<div class="row">
          <div class="col-3">
          @include('user.students._nav')
          </div>
          <div class="col-9">

          <div class="card card-primary">
              <div class="card-header">
              <a href="/students/{{ $student->id }}/academic?show=add-form" class="float-right"><i class="fa fa-plus"></i></a>
                <h2 class="card-title"><strong>ACADEMIC INFORMATION</strong></h2><br>
                <small></small>
              </div>
              <!-- /.card-header -->
              <!-- form start -->

                <div class="card-body">

@foreach($student->academics as $academic)
                  <div class="callout callout-info">
                      {{ $academic->last_attended }}, {{ $academic->address }} <span class="badge badge-info">{{ config('enrollment.grade_levels')[$academic->grade_level]['name'] }} - {{ $academic->school_year }}</span>
                      <a href="/students/{{ $academic->id }}/edit_academic">
                      <i class="fa fa-pencil float-right"></i>
                    </a>
                  </div>
@endforeach

                </div>

          </div>

@if( Request::query('show')=='add-form' )

<form method="POST">
@csrf
<div class="card card-warning">
              <div class="card-header">
                <h2 class="card-title"><strong><i class="fa fa-plus"></i> ADD NEW</strong></h2><br>
                <small></small>
              </div>
<div class="card-body">

<div class="row">
<div class="col-8">
                  <div class="form-group">
                    <label for="last_attended">Last School Attended</label>
                    <input name="last_attended" value="" type="text" class="form-control" id="last_attended" placeholder="Enter Last School Attended">
                  </div>
</div>
<div class="col-4">
                  <div class="form-group">
                    <label for="grade_level">Grade Level</label>
                    <select class="form-control" name="grade_level">
                      @foreach(config('enrollment.grade_levels') as $key => $level)
                        <option value="{{ $key }}">{{ $level['name'] }}</option>
                      @endforeach
                    </select>
                  </div>
</div>
<div class="col-8">
                  <div class="form-group">
                    <label for="address">School Address</label>
                    <input name="address" value="" type="text" class="form-control" id="address" placeholder="Enter School Address">
                  </div>
</div>
<div class="col-4">
                  <div class="form-group">
                    <label for="school_year">School Year</label>
                    <input name="school_year" value="" type="text" class="form-control" id="school_year" placeholder="Enter School Year">
                  </div>
</div>
</div>


                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              
            </div>

</form>

@endif
 




          </div>
          <!-- /.col -->
</div>
<!-- /.row -->
        
</div>
</section>
@endsection
