@extends('user.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Student
            <span class="badge badge-success">{{ $student->lastname }}, {{ $student->firstname }}</span>
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/students">My Students</a></li>
              <li class="breadcrumb-item active">Edit Student</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">
<div class="container-fluid">

<div class="row">
          <div class="col-3">
          @include('user.students._nav')
          </div>
          <div class="col-9">

          <div class="card card-primary">
              <div class="card-header">
                <h2 class="card-title"><strong>PERSONAL INFORMATION</strong></h2><br>
                <small></small>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post">
              @csrf
                <div class="card-body">

<div class="row">
<div class="col-4">
                  <div class="form-group">
                    <label for="last_name">Last Name *</label>
                    <input value="{{ $student->lastname }}" type="text" class="form-control" name="last_name" id="last_name" placeholder="Enter Last Name">
                  </div>
</div>
<div class="col-4">
                  <div class="form-group">
                    <label for="first_name">First Name *</label>
                    <input value="{{ $student->firstname }}" type="text" class="form-control" name="first_name" id="first_name" placeholder="Enter First Name">
                  </div>
</div>
<div class="col-4">
                  <div class="form-group">
                    <label for="middle_name">Middle Name *</label>
                    <input value="{{ $student->middlename }}" type="text" class="form-control" name="middle_name" id="middle_name" placeholder="Enter Middle Name">
                  </div>
</div>
</div>

<div class="row">
<div class="col-6">
                  <div class="form-group">
                    <label for="birth_date">Birth Date</label>
                    <input value="{{ date('m/d/Y', strtotime($student->birthday)) }}" type="text" class="form-control" name="birth_date" id="birth_date" placeholder="Enter Birth Date">
                  </div>
</div>
<div class="col-6">
                  <div class="form-group">
                    <label for="birth_place">Birth Place</label>
                    <input value="{{ $student->birthplace }}" type="text" class="form-control" name="birth_place" id="birth_place" placeholder="Enter Birth Place">
                  </div>
</div>
<div class="col-6">
                  <div class="form-group">
                    <label for="religion">Religion</label>
                    <input value="{{ $student->religion }}" type="text" class="form-control" name="religion" id="religion" placeholder="Enter Religion">
                  </div>
</div>
<div class="col-6">
                  <div class="form-group">
                    <label for="middle_name">Gender</label>
                    
                    <div class="row">
                        <div class="col-6">
                            <div class="custom-control custom-radio">
                            <input {{ ($student->gender=='m') ? 'CHECKED' : '' }} class="custom-control-input" type="radio" id="genderMale" name="gender" value="m">
                            <label for="genderMale" class="custom-control-label">Male</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="custom-control custom-radio">
                            <input {{ ($student->gender=='f') ? 'CHECKED' : '' }} class="custom-control-input" type="radio" id="genderFemale" name="gender" value="f">
                            <label for="genderFemale" class="custom-control-label">Female</label>
                            </div>
                        </div>
                    </div>

                  </div>
</div>
</div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
<small>* Name must based on the PSA/NSO Birth Certificate</small>

 




          </div>
          <!-- /.col -->
</div>
<!-- /.row -->
        
</div>
</section>
@endsection
