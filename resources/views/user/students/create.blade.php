@extends('user.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add a Student</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/students">My Students</a></li>
              <li class="breadcrumb-item active">Add a Student</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">
<div class="container">

<div class="row">
          <div class="col-12">

          <div class="card card-primary">
              <div class="card-header">
                <h2 class="card-title"><strong>PERSONAL INFORMATION</strong></h2><br>
                <small>Name must based on the PSA/NSO Birth Certificate</small>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">

<div class="row">
<div class="col-4">
                  <div class="form-group">
                    <label for="last_name">Last Name</label>
                    <input type="text" class="form-control" id="last_name" placeholder="Enter Last Name">
                  </div>
</div>
<div class="col-4">
                  <div class="form-group">
                    <label for="first_name">First Name</label>
                    <input type="text" class="form-control" id="first_name" placeholder="Enter First Name">
                  </div>
</div>
<div class="col-4">
                  <div class="form-group">
                    <label for="middle_name">Middle Name</label>
                    <input type="text" class="form-control" id="middle_name" placeholder="Enter Middle Name">
                  </div>
</div>
</div>

<div class="row">
<div class="col-3">
                  <div class="form-group">
                    <label for="birth_date">Birth Date</label>
                    <input type="text" class="form-control" id="birth_date" placeholder="Enter Birth Date">
                  </div>
</div>
<div class="col-3">
                  <div class="form-group">
                    <label for="birth_place">Birth Place</label>
                    <input type="text" class="form-control" id="birth_place" placeholder="Enter Birth Place">
                  </div>
</div>
<div class="col-3">
                  <div class="form-group">
                    <label for="religion">Religion</label>
                    <input type="text" class="form-control" id="religion" placeholder="Enter Religion">
                  </div>
</div>
<div class="col-3">
                  <div class="form-group">
                    <label for="middle_name">Gender</label>
                    
                    <div class="row">
                        <div class="col-6">
                            <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="genderMale" name="gender">
                            <label for="genderMale" class="custom-control-label">Male</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="genderFemale" name="gender">
                            <label for="genderFemale" class="custom-control-label">Female</label>
                            </div>
                        </div>
                    </div>

                  </div>
</div>
</div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>


          </div>
          <!-- /.col -->
</div>
<!-- /.row -->
        
</div>
</section>
@endsection
