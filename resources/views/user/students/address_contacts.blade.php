@extends('user.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Student <span class="badge badge-success">{{ $student->lastname }}, {{ $student->firstname }}</span></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/students">My Students</a></li>
              <li class="breadcrumb-item active">Edit Student</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">
<div class="container-fluid">

<div class="row">
          <div class="col-3">
          @include('user.students._nav')
          </div>
          <div class="col-9">

          <div class="card card-primary">
              <div class="card-header">
                <a href="/students/{{ $student->id }}/address_contacts?show=add-form" class="float-right"><i class="fa fa-plus"></i></a>
                <h2 class="card-title"><strong>ADDRESS &amp; CONTACTS</strong></h2><br>
                <small></small>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
  
<div class="card-body">


@foreach($student->addresses as $address)
                  <div class="callout callout-info">
                      {{ $address->address }} <span class="badge badge-info">{{ $address->zip }}</span>
                      <a href="/students/{{ $address->id }}/edit_address">
                      <i class="fa fa-pencil float-right"></i>
                    </a>
                  </div>
@endforeach



</div>

</div>

@if( Request::query('show')=='add-form' )

<form method="POST">
@csrf
<div class="card card-warning">
              <div class="card-header">
                <h2 class="card-title"><strong><i class="fa fa-plus"></i> ADD NEW</strong></h2><br>
                <small></small>
              </div>
<div class="card-body">

<div class="row">
          <div class="col-10">
                  <div class="form-group">
                    <label for="address">Address</label>
                    <input value="" type="text" class="form-control" name="address" id="address" placeholder="Enter Address">
                  </div>
          </div>
          <div class="col-2">
                  <div class="form-group">
                    <label for="zip">Zip Code</label>
                    <input value="" type="text" class="form-control" name="zip" id="zip" placeholder="Enter Zip">
                  </div>
          </div>
</div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
             
            </div>
</form>
@endif

          </div>
          <!-- /.col -->
</div>
<!-- /.row -->
        
</div>
</section>
@endsection
