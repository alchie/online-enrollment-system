<div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
@foreach([
    'personal' => 'Personal Infomation',
    'address_contacts' => 'Address & Contacts',
    'academic' => 'Academic Infomation',
    'family' => 'Family Infomation',
] as $key=>$value)
    <a class="nav-link {{ ($current_key==$key) ? 'active' : '' }}" href="/students/{{ $student->id ?? $student_id }}/{{ $key }}">{{ $value }}</a>
@endforeach
</div>