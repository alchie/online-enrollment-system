@extends('user.layouts.app')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>My Students

<div class="btn-group" role="group" aria-label="Button group with nested dropdown">
  <div class="btn-group" role="group">
    <button id="btnGroupDrop1" type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="fa fa-plus"></i> Add a Student
    </button>
    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
      <a class="dropdown-item" href="/students/create">Create New Profile</a>
      <a class="dropdown-item" href="/students/claim">Claim Old Profile</a>
    </div>
  </div>
</div>

            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item active">My Students</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="content">
<div class="container">

<div class="row">

@foreach($students as $student) 
<div class="col-md-4">
            <!-- Widget: user widget style 2 -->
            <div class="card card-widget widget-user-2">
            
              <div class="widget-user-header bg-gray">
              
              <a href="/students/{{ $student->student_id }}/personal" class="btn btn-dark btn-xs float-right"><i class="fa fa-pencil"></i></a>
               
                <div class="widget-user-image">
                  <img class="img-circle elevation-2" src="/storage/img/{{ ( $student->gender=='m' ) ? 'avatar' : 'avatar2' }}.png" alt="User Avatar">
                </div>
                <!-- /.widget-user-image -->
                <h3 class="widget-user-username">{{ $student->information->firstname }}</h3>
                <h5 class="widget-user-desc">{{ $student->information->lastname }}</h5>
              </div>
              <div class="card-footer">

<a href="/students/{{ $student->student_id }}/personal"  class="badge badge-success">
Personal Information 
</a>
<a href="/students/{{ $student->student_id }}/address_contacts"  class="badge badge-success">
Address & Contacts
</a>
<a href="/students/{{ $student->student_id }}/academic"  class="badge badge-success">
Academic Information
</a>
<a href="/students/{{ $student->student_id }}/family"  class="badge badge-success">
Family Information 
</a>
                
              </div>
            </div>
            <!-- /.widget-user -->
          </div>
          <!-- /.col -->
@endforeach
</div>

{{ $students->links() }}

<!-- /.row -->    
</div>
</section>
@endsection
